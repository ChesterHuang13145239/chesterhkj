import os 
import glob
import json
import sys
import re
import argparse

import maptodf
import resulttodf

def Argparse():

    example = (
        "Command Example: \n"
        "python autoeval.py --pic_name 20201104-part2 --tool ../../\n"
        )
        
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter, epilog=example)
    parser.add_argument("-p", "--pic_name", type=str, help="pic_name")
    parser.add_argument("-t", "--tool", type=str, help="pmxtoolpath")

    args = parser.parse_args()
    return args


def printstatus(msg, colorcode=33):
    """Creates block to hightlight the message.
    default 33: yellow
            35: red
    """

    print("\n"+"="*50)
    print("\n\033[1;{}m{:^50s}\033[0m".format(colorcode, msg))
    print("="*50 +"\n")


def printinfo(msg, colorcode=33, enhanced=1, background=0):
    """Formats string for looking good on terminal.
    default 33: yellow
    """

    print("\033[{};{};{}m{}\033[0m".format(enhanced, background, colorcode, msg))


def dircheck(dirpath, makedir=False):
    if not os.path.isdir(dirpath):
        if makedir:
            os.makedirs(dirpath)
        else:
            raise NotADirectoryError("{} isn't exist.".format(dirpath))

    return os.path.abspath(dirpath) #for chdir


def patternfind(pattern, string):

    pattern = re.compile(pattern)
    target = pattern.findall(string)
    if target:
        return target[0]

    return target


def rawlog_collect(picname, rawlogpath):
    '''
    # picname: 20201104-part2
    # rawlogpath: ./rawlog/

    # name: Bins_20201104-part2_ssd_v6.1.1_640x360_residualv2.0-retrain4-46k.txt
    # resolution: 640x360
    # backbone: residualv2.0
    # traininground: retrain4
    # dest: ./rawlog/20201104-part2/640x360/
    '''

    rawloglist = glob.glob("./*{}*.txt".format(picname))

    for rawlog in rawloglist:
        name = os.path.basename(rawlog)
        resolution = patternfind(r"\d+x\d+", name)
        # backbone ,traininground = name.split("_")[-1].split("-")[0:2]
        dest = os.path.join(rawlogpath, resolution)
        
        if not os.path.isdir(dest):
            os.makedirs(dest)

        dest = os.path.join(dest, name)
        print(name)
        os.system("mv {} {}".format(rawlog, dest))
        # os.system("cp {} {}".format(rawlog, dest))


def rawlogtossdlog(rawlogpath, ssdlogpath, picname, pwd):
    '''
    # rawlogpath: ./rawlog/20201104-part2/
    # ssdlogpath: ./ssdlog/20201104-part2/
    # picname: 20201104-part2

    # rawlog: ./rawlog/20201104-part2/640x360/Bins_20201104-part2_ssd_v6.1.1_640x360_residualv2.0-retrain4-46k.txt
    # dirtree: 640x360
    # ssdname: 20201104-part2_ssd_v6.1.1_640x360_residualv2.0-retrain4-46k.txt
    # dest: ./ssdlog/20201104-part2/640x360
    '''

    rawloglist = glob.glob("{}/**/*.txt".format(rawlogpath), recursive=True)
    # print(rawloglist)

    for rawlog in rawloglist:
        name = os.path.basename(rawlog)
        rawlogdir = os.path.dirname(rawlog)
        dirtree = patternfind(r"\d+x\d+.*", rawlogdir)
        dest = os.path.join(ssdlogpath, dirtree)

        if not os.path.isdir(dest):
            os.makedirs(dest)

        ssdname = "_".join(name.split("_")[1:])
        dest = os.path.join(dest, ssdname)
        printinfo(ssdname)
        os.system("python3 convertSSDLogfromRaw.py --raw_path={} --save_name={}".format(rawlog, dest))
        # print("count", rawlog)
        # print(1/0)
        printinfo("Count runtime")
        printinfo(name)
        os.system("python3 countruntime.py {} |tee {}/roundtime.txt".format(rawlog, pwd))


def runevaluate(ssdlogpath, yolopath, gtpath, picpath, resultpath, pwd):
    '''
    # ssdlogpath: ./ssdlog/20201104-part2/
    # yolopath: ./yolo/20201104-part2/
    # gtpath: ./gt/20201104-part2/
    # picpath: ./pic/20201104-part2/
    # resultpath: ./result/20201104-part2/
    # pwd: pmx_tools/AI_Evaluation/autoeval/03_Evaluate

    # ssdlog: ./ssdlog/20201104-part2/640x360/20201104-part2_ssd_v6.1.1_640x360_residualv2.0-retrain4-46k.txt
    # dirtree: 640x360
    # dest: ./result/20201104-part2/640x360/
    '''

    ssdloglist = glob.glob("{}/**/*.txt".format(ssdlogpath), recursive=True)
    yololog = glob.glob("{}/*.txt".format(yolopath))[0]

    for ssdlog in ssdloglist:
        ssdlogdir = os.path.dirname(ssdlog)
        dirtree = patternfind(r"\d+x\d+.*", ssdlogdir)
        dest = os.path.join(resultpath, dirtree)

        resultname = os.path.join(dest, os.path.basename(ssdlog).replace(".txt","")+"_Eval_draw.txt")
        if os.path.exists(resultname):
            continue

        if not os.path.isdir(dest):
            os.makedirs(dest)

        if os.path.isdir("outPut"):
            os.system("rm -rf outPut")
        os.makedirs("outPut", exist_ok=True)

        os.system("echo 0 |python3 evaluatePrecisionRecall.py \
            --ssd_path={} --yolo_path={} --gt_path={} --pic_path={} --resolution=[1920,1080] --draw_line=Read".format(ssdlog, yololog, gtpath, picpath))

        os.system("mv outPut/* {}".format(dest))

    os.chdir(pwd)
    resultlist = glob.glob("{}/*".format(resultpath))
    for d in resultlist:
        resulttodf.main(d)


def runmAP(ssdlogpath, gtpath, picpath, mAPpath, pwd):
    '''
    # ssdlogpath: ./ssdlog/20201104-part2/
    # gtpath: ./gt/20201104-part2/
    # picpath: ./pic/20201104-part2/
    # mAPpath: ./mAP/20201104-part2/
    # pwd: pmx_tools/AI_Evaluation/autoeval/03_Evaluate

    # ssdlog: ./ssdlog/20201104-part2/640x360/20201104-part2_ssd_v6.1.1_640x360_residualv2.0-retrain4-46k.txt
    # dirtree: 640x360
    # dest: ./mAP/20201104-part2/640x360/

    '''

    ssdloglist = glob.glob("{}/**/*.txt".format(ssdlogpath), recursive=True)

    for ssdlog in ssdloglist:
        name = os.path.basename(ssdlog)
        ssdlogdir = os.path.dirname(ssdlog)
        dirtree = patternfind(r"\d+x\d+.*", ssdlogdir)
        dest = os.path.join(mAPpath, dirtree)
        if not os.path.isdir(dest):
            os.makedirs(dest)

        savename = name.replace(".txt", "") + "-mAP.txt"
        savename = os.path.join(dest, savename)
        if os.path.exists(savename):
            continue

        os.system("python3 mAP_processing.py \
            --gt_path={} --ai_path={} --pic_path={} --resolution=[1920,1080] -a | tee {} ".format(gtpath, ssdlog, picpath, savename))

    os.chdir(pwd)
    mAPlist = glob.glob("{}/*".format(mAPpath))
    for d in mAPlist:
        maptodf.main(d)


if __name__ == '__main__':

    args = Argparse()
    
    picname = args.pic_name # sys.argv[1]

    rawlogpath = dircheck("./rawlog/{}".format(picname), makedir=True)
    gtpath = dircheck("./gt/{}".format(picname))
    yolopath = dircheck("./yolo/{}".format(picname))
    picpath = dircheck("./pic/{}".format(picname))
    ssdlogpath = dircheck("./ssdlog/{}".format(picname), makedir=True)
    resultpath = dircheck("./result/{}".format(picname), makedir=True)
    mAPpath = dircheck("./mAP/{}".format(picname), makedir=True)

    pmxtoolpath = args.tool # sys.argv[2]
    pwd = os.getcwd()

    printstatus("rawlog_collect")
    rawlog_collect(picname, rawlogpath)

    printstatus("rawtossd")
    os.chdir(pmxtoolpath)
    rawlogtossdlog(rawlogpath, ssdlogpath, picname, pwd)

    printstatus("evaluate precision recall")
    runevaluate(ssdlogpath, yolopath, gtpath, picpath, resultpath,pwd)

    printstatus("calculate mAP")
    os.chdir(pmxtoolpath)
    runmAP(ssdlogpath, gtpath, picpath, mAPpath, pwd)
    





    

    



