import json
import glob
import sys
import os 
from collections import OrderedDict

def mkloop(loopfile, pic_dataset, config):
    with open(loopfile, "a") as f:
        if config["additional"]:
            rawlogname = "Bins_{}_{}_{}-{}-{}{}.txt".format(pic_dataset, 
                                                            config["Network Name"], 
                                                            config["backbone"], 
                                                            config["training_round"], 
                                                            config["training_iter"], 
                                                            config["additional"])
        else:
            rawlogname = "Bins_{}_{}_{}-{}-{}.txt".format(pic_dataset, 
                                                          config["Network Name"], 
                                                          config["backbone"], 
                                                          config["training_round"], 
                                                          config["training_iter"])


        cmd = "sh ./Run_bins.sh /sdcard/{} /sdcard/{} \n".format(config["cavalry"], config["priorbox"])
        cmd2 = "mv Bins.txt {} \n".format(rawlogname)
        cmdecho = "echo {} finish \n".format(rawlogname)
        f.write(cmd)
        f.write(cmd2)
        f.write(cmdecho)
    print(rawlogname)

if __name__ == '__main__':
    
    pic_dataset = sys.argv[1]
    
    loopfile = "{}_loop.sh".format(pic_dataset)
    if os.path.isfile(loopfile):
        os.remove(loopfile)

    configlist = glob.glob("Working/**/convert_config.json", recursive=True)

    for configfile in configlist:
        with open(configfile, "r") as f:
            config = json.load(f, object_pairs_hook=OrderedDict)

            mkloop(loopfile, pic_dataset, config)
