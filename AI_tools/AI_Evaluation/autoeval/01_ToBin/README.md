# 01_ToBin

### 前置動作
1. 建立Model_to_bin軟連結
2. 建立NN_training軟連結
3. 建立todolog.txt
4. 將目標圖片資料集轉Bin

### 步驟一：Run readlog.py
#### 執行
```
python readlog.py todolog.txt
```

#### 介紹
讀取todolog.txt中的caffemodel訓練log檔。

擷取其中每個iteration的分數，整理成DataFrame並輸出成CSV。

將其分割成4個區間，每個區間取前5高，最後選出20個model。

#### I/O
輸入：todolog.txt 

輸出： 
1. 該次訓練的所有分數CSV
2. 被選出的20名分數CSV (\_select)
3. todomodel.txt

>todolog.txt
```
./NN_training/caffe/PrimaxSDD_v6.1.1/PrimaxSSD_residualv2.0/retrain4/log20220211
./NN_training/caffe/PrimaxSDD_v6.1.1/PrimaxSSD_residualv2.0/retrain4a/log20220214
```

>todmodel.txt
```
./NN_training/caffe/PrimaxSDD_v6.1.1/PrimaxSSD_residualv2.0/retrain4a/snapshot/_iter_36000.caffemodel
./NN_training/caffe/PrimaxSDD_v6.1.1/PrimaxSSD_residualv2.0/retrain4a/snapshot/_iter_27000.caffemodel
./NN_training/caffe/PrimaxSDD_v6.1.1/PrimaxSSD_residualv2.0/retrain4a/snapshot/_iter_17000.caffemodel
......(略)
```

### 步驟二：Run toworking.py
#### 執行
```
python toworking.py --todo_list=todomodel.txt --pic_name=20201104-part2
```

### 介紹
讀取todomodel.txt中的caffemodel路徑，根據caffemodel路徑獲得模型資訊，再根據模型資訊整理所挑選出的模型，將模型資訊存入convert_config.json。

將模型轉換成bin檔時可以讀取convert_config.json中的資訊做輸入。

整理完成後會執行run_convert.py將caffemodel轉成bin。

生成此todomodel.txt於guadianII上可一鍵執行的bash檔loop.sh。

#### I/O
輸入：todomodel.txt

輸出：
1. ./Working/AMBA3.0/PrimaxSSD\_{version}\_{backbone}/{networkname}\_{backbone}\_{training_round}\_{training_iter}
2. toGuardianII/



+ Working
    * AMBA3.0
        * PrimaxSSD\_{version}\_{backbone}
            * {networkname}\_{backbone}\_{training_round}\_{training_iter}
                 * convert_config.json
                 * priorbox_fp32.bin
                 * cavalry_ssd_{version}\_{resolution}\_{backbone}-{training_round}-{training_iter}.bin
                 * \_iter_{training_iter}.caffemodel
                 * deploy.prototxt


toGuardianII
+ {pic_name}\_loop.sh
+ Working/AMBA3.0/PrimaxSSD\_{version}\_{backbone}/{networkname}\_{backbone}\_{training_round}\_{training_iter}
    + priorbox_fp32.bin
    + cavalry_ssd_{version}\_{resolution}\_{backbone}-{training_round}-{training_iter}.bin


>convert_config.json
```
{
 "Network Name": "ssd_v6.1.1_640x360",
 "Input Width": "640",
 "Input Height": "360",
 "Input Data Format": "0,0,0,0",
 "Meaning or No Meaning": "1",
 "DRA Strategy": "1",
 "Scale or No Scale": "1",
 "Output Format": "0",
 "Output Number": "2",
 "Output Name1": "mbox_loc",
 "Output Name2": "mbox_conf_flatten",
 "Run cavalry_gen": "0",
 "backbone": "residualv2.0",
 "caffemodel": "_iter_17000.caffemodel",
 "deploy": "deploy.prototxt",
 "training_round": "retrain4",
 "training_iter": "17k",
 "img_path": "img/",
 "dest": "./Working/AMBA3.0/PrimaxSSD_v6.1.1_residualv2.0/ssd_v6.1.1_640x360_residualv2.0_retrain4_17k",
 "additional": [],
 "cavalry": "./Working/AMBA3.0/PrimaxSSD_v6.1.1_residualv2.0/ssd_v6.1.1_640x360_residualv2.0_retrain4_17k/cavalry_ssd_v6.1.1_640x360_residualv2.0-retrain4-17k.bin",
 "priorbox": "./Working/AMBA3.0/PrimaxSSD_v6.1.1_residualv2.0/ssd_v6.1.1_640x360_residualv2.0_retrain4_17k/priorbox_fp32.bin"
}
```


### 步驟三：Copy toGuardianII
將toGuardianII的檔案從server上抓下來

---


# Optional
### Run mkloop.py
#### 執行
```
python mkloop.py 20201104-part2
```

#### 介紹
讀取Working資料夾中的資料，生成不同圖片資料集用的loop.sh

