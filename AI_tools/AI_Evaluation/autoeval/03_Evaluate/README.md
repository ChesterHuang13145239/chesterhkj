# 03_Evaluate

### 前置作業
1. 建立gt、pic、yolo資料夾中對應的資料及軟連結

>以20201104-part2資料集為例
+ gt
	+ 20201104-part2
		+ 20201104-part2_00000.xml
		+ ....

+ pic
	+ 20201104-part2
		+  20201104-part2_00000.jpg
		+ ....

 + yolo
	 + 20201104-part2
		 + 20201104-part2_yolo_608x608.txt

2. 將rawlog複製於autoeval.py同層

### 步驟一：Run autoeval.py
#### 執行
```
python autoeval.py --pic_name=20201104-part2 --tool=../../
```
+ pic_name：要處理的資料集名稱
+ tool：AI_Evaluation資料夾的路徑

#### 介紹
共有四個功能。
1. 整理rawlog存到對應的資料夾分層。
2. run convertSSDLogfromRaw.py，結果存放至對應的資料夾
3. run evaluatePrecisionRecall.py，結果存放至對應的資料夾，並生成好讀版CSV
4. run mAP_preprocessing.py auto模式，結果存放至對應的資料夾，並生成好讀版CSV

#### I/O
輸入：rawlog、gt、pic、yololog

輸出：
1. rawlog/
2. ssdlog/
3. result/
4. mAP/

### 步驟二：收集資料

收集填寫Excel所需資料。

1. 01.ToBin 的 \_seclect.csv
2. result資料夾中的 result.csv
3. mAP資料夾中的 mAP.csv


