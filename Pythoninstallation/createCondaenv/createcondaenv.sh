#!/bin/bash
# Author: Chester Huang
#
# This is a script used to install anaconda environment and python
# packages on MacOS via anaconda/pip3.
#

#
# parse arguments
#
help()
{
  echo "Options of this program are listed below."
  echo
  echo "Syntax: scriptTemplate [-n|v|l|h]"
  echo "Options:"
  echo "-n   --env_name   The name of new conda environment."
  echo "-v   --version    The version of Python used in new conda environment."
  echo "-l   --list       The file lists all Python packages need to be installed."
  echo "-h   --help       Print help message."
  echo
  echo "Command Example:"
  echo "sh createcondaenv.sh --env_name test --version 3.8 --list ./cfg/cfg_python38.txt"
}

while [[ $# -gt 0 ]];
do
  case $1 in
    -n|--env_name)
      env_name="$2"
      shift
      shift
      ;;
    -v|--version)
      py_version="$2"
      shift
      shift
      ;;
    -l|--list)
      package_list="$2"
      shift
      shift
      ;;
    -h|--help)
      help
      exit 0
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
  esac
done

workdir=$(dirname "$0")
script_name=$(basename "$0")
start_time=$(date +%Y%m%d-%H:%M:%S)

#
# temporarily disable time duration calculation as
# some kind of syntax error on Ubuntu.
# Error signautre:
# date: invalid date ‘20230827-10:32:19’
# date: invalid date ‘20230827-10:30:48’
# ./createcondaenv.sh: line 155: (-)/60: syntax error: operand expected (error token is ")/60")

#
#if [[ "${SHELL}" == "/bin/zsh" ]]; then
#  start_point=$(date +%s)
#else
#  start_point=$(date +%s)
#fi
echo "[${script_name}]  start time: ${start_time}"
#
# create anaconda environment with specific version of python
#
echo "[${script_name}]  start to create anaconda environment: \
environment_name=${env_name} python_version=Python${py_version} "

conda create --name "${env_name}" python="${py_version}" -y
create_rc=$?
echo "create_rc: ${create_rc}"

if [ ${create_rc} -eq 0  ]; then
  echo "[${script_name}]  \033[;32;1mSUCCESS\033[0m  : create anaconda environment ${env_name}"
else
  echo "[${script_name}]  \033[;31;1mFAILURE\033[0m  : Unable to create anaconda environment ${env_name}"
fi

#
# source conda.sh to avoid conda init issue
# refs: https://github.com/conda/conda/issues/7980
#
if [[ "${SHELL}" == "/bin/zsh" ]]; then
  source /usr/local/Caskroom/miniforge/base/etc/profile.d/conda.sh
else
  source activate
  conda deactivate
fi


#
# activate conda environment
#
echo "[${script_name}]  try to activate anaconada environment ${env_name}"
conda activate "${env_name}"
activate_rc=$?

if [ ${activate_rc} -eq 0  ]; then
  echo "[${script_name}]  \033[;32;1mSUCCESS\033[0m  : activate anaconda environment ${env_name}"
else
  echo "[${script_name}]  \033[;31;1mFAILURE\033[0m  : Unable to activate anaconda environment ${env_name}"
  exit 1
fi


#
# install python packages by reading package list
#
echo "[${script_name}]  start to install python packages via anaconda"

state=0
package_name=""
package_cmd=""
while read line; do
  if [[ "${line}" =~ ^#.* ]]; then
    continue
  fi

  if [[ "${state}" -eq 0 ]]; then
    if [[ "${line}" != "" ]]; then
      package_name="${line}"
      state=1
    fi
  elif [[ "${state}" -eq 1 ]]; then
    if [[ "${line}" != "" ]]; then
      echo "[${script_name}]  start to install ${package_name}."
      package_cmd="${line}"
      state=2
    else
      echo "[${script_name}]  ERROR  : no cmd to install package ${package_name}"
      exit 1
    fi
  elif [[ "${state}" -eq 2 ]]; then
    if [[ "${line}" != "" ]]; then
      package_cmd="${package_cmd} ${line}"
      state=2
    else
      state=0
      #eval ${package_cmd}
    fi
  fi
done < "${package_list}"


#
# deactivate conda environment
#
echo "[${script_name}]  deactivate anaconda environment: ${env_name}"
conda deactivate

end_time=$(date +%Y%m%d-%H:%M:%S)
#if [[ "${SHELL}" == "/bin/zsh" ]]; then
#  end_point=$(date +%s)
#else
#  end_point=$(date +%s)
#fi


#
# Show time duration of creating conda environment
#
#if [[ "${SHELL}" == "/bin/zsh" ]]; then
#  duration_s=$((((${end_point} - ${start_point}))/60))
#else
#  duration_s=$((($(date --date="${end_point}")-$(date --date="${start_point}"))/60))
#fi


echo "[${script_name}]  end time: ${end_time}"
#echo "[${script_name}]  duration: ${duration_s} mins"
