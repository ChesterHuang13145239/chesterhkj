Load log from ../example/logs/20201104-part2_ssd_v5.1.1_640x360_Data20210420-retrain5b-160k.txt.
Load pics from ../example/pics/20201104-part2.
Load log from ../example/logs/20201104-part2_yolo_608x608.txt.
Load pics from ../example/pics/20201104-part2.
Read xml files from ../example/GT/20201104-part2.
vertex_list:  None

======== SSD vs. YoloV4 ========

 Create Table 
 Count AI in GT: 4485


 Precision: 0.887698986975398
 Recall: 0.6414975946454716
 TP: 3067
 TP_TABLE: [411, 1039, 1617, 0]
 AI_TABLE: [469, 1135, 1849, 2]
 GT_TABLE: [479, 1382, 2623, 1]
 sum_AI: 3455
 sum_GT: 4781

RESULTS:
                  Precision     Recall
Bike        ->    0.88    	0.86
Vehicle     ->    0.92    	0.75
Person      ->    0.87    	0.62

======== SSD vs. GT ========

 Create Table 
 Count AI in GT: 612


 Precision: 0.8821052631578947
 Recall: 0.684640522875817
 TP: 419
 TP_TABLE: [60, 142, 217, 0]
 AI_TABLE: [65, 157, 253, 0]
 GT_TABLE: [69, 171, 372, 0]
 sum_AI: 475
 sum_GT: 612

RESULTS:
                  Precision     Recall
Bike        ->    0.92    	0.87
Vehicle     ->    0.90    	0.83
Person      ->    0.86    	0.58

======== YoloV4 vs. GT ========

 Create Table 
 Count AI in GT: 612


 Precision: 0.8724727838258165
 Recall: 0.9166666666666666
 TP: 561
 TP_TABLE: [63, 171, 327, 0]
 AI_TABLE: [65, 185, 353, 0]
 GT_TABLE: [69, 171, 372, 0]
 sum_AI: 643
 sum_GT: 612

RESULTS:
                  Precision     Recall
Bike        ->    0.97    	0.91
Vehicle     ->    0.92    	1.00
Person      ->    0.93    	0.88
