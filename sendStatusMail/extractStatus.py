#!/usr/local/Caskroom/miniforge/base/envs/WorkSpace3.8
#
# This script is used to exatrct imformation from json file, 
# and tries to create content of email. After that, we will 
# use another script to send this email to appointed receivers.
#

import os
import argparse
import json
import re
import datetime

SCRIPT_NAME = "\033[92mextractStatus\033[0m"

def argParse():

    example = (
              "Command exmaple: \n"
              "python3 extractStatus.py --file_path <file_path>"
    )

    parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter,
                                     epilog = example)
    parser.add_argument("-sf", "--statusfile", type = str, help = "The file path of round status.")
    parser.add_argument("-uf", "--userfile", type = str, help = "The file path of user information.")
    args = parser.parse_args()

    return args

def loadJsonfile(jsonfile: str):

    data = {}

    with open(jsonfile, "r") as f:
        data = json.load(f)
        f.close()

    return data

def parseFailedstatus(statusfile: str):

    statusdata   = {}
    passedround  = []
    failedround  = []
    skippedround = []

    statusdata = loadJsonfile(statusfile)
    rounds     = statusdata["round_status"]
    for round in rounds:
        rounddata    = {}
        rounduser    = ""
        roundresult  = ""
        roundruntime = ""

        rounddata    = rounds[round]
        rounduser    = rounddata.get("user")
        roundresult  = rounddata.get("result")
        roundruntime = rounddata.get("runtime")

        if roundresult == "Passed":
            passedround.append(round)
        elif roundresult == "Failed":
            failedround.append(round)
        elif roundresult == "Skipped":
            skippedround.append(round)
        else:
            print(f"[{SCRIPT_NAME}]  \033[91mWARNING\033[0m  : Having unknown results in {statusfile}.")
            print(f"Round = {round},\nUser = {rounduser},\nResult = {roundresult},\nRuntime = {roundruntime}")

    roundstatusmessage = "Passed Round: " + ",".join(passedround) + "\n "\
                         "Failed Round: " + ",".join(failedround) + "\n "\
                         "Skipped Round: " + ",".join(skippedround)

    if len(failedround) > 0:
        print(f"[{SCRIPT_NAME}]  Round Status:\n", roundstatusmessage)
        return failedround
    else:
        return None

def createMailInfo(userdata: dict, failedround: list):

    mailinfo = []

def main():

    args = argParse()
    statusfile = args.statusfile
    userfile   = args.userfile

    userdata    = loadJsonfile(userfile)
    print(f"[{SCRIPT_NAME}]  UserData: ", userdata)
    roundstatus = parseFailedstatus(statusfile)

    if roundstatus != None:
        createMailInfo(userdata, roundstatus)
    else:
        print(f"[{SCRIPT_NAME}]  INFO: No failed rounds.")
        exit()

if __name__=="__main__":
    main()
