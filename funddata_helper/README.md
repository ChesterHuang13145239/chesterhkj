# funddata_helper

## Introduction
funddata_helper is the project to develope tools search fund data and transfer those data into different forms.

## Features
- Search fund data through crawler and store those data into database.
- Compare current data with previous data of appointed funds and send notification
- Show data and corresponding graph on web browser through HTML
- Buttons which can redirect to related web page

## Dependency
- Python
- HTML
- SQL

## Work Flow

## Execution

## Reference
