import os
import serial

import datetime

from datetime import datetime

from serial.tools import list_ports

from PyQt5.QtCore import (QThread, 
                          pyqtSignal,
                          QTimer
                          )

from config import (DATABITS_INPUT_VALUE, \
                    PARITY_INPUT_VALUE, \
                    STOPBITS_INPUT_VALUE, \
                    FLOWCONTROL_INPUT_VALUE
                    )

class Worker(QThread):
    showmessage = pyqtSignal(str)

    def __init__(self, parent = None):
        super(Worker, self).__init__(parent)
        self.ser = None
        self.portdict = {}
        self.serconnectstatus = False
        self.timer = QTimer()
        self.datanum = 0
        self.logfile = str

        self.getPortDict()
        
    def run(self):

        self.showmessage.emit("system start!")

    def getPortDict(self):
        # This function is used to store information of ports
        # which are detected by system with dict structure.

        portlist = serial.tools.list_ports.comports()
        for port in portlist:
            device = port.device
            name   = port.name
            descr  = port.description
            hwid   = port.hwid

            portinfo = [device, name, descr, hwid]
            self.portdict[device] = portinfo

    def controlPortConnection(self, settingvalues, command):
        # This function defines the operation under different
        # commands("Connect" andf "Disconnect").

        if command == "Connect":
            self.openPort(settingvalues)

            if self.ser.isOpen() == True:

                self.showmessage.emit("======= Serial Connection Info ======")
                self.showmessage.emit("Port: {}".format(self.ser.port))
                self.showmessage.emit("Baudrate: {}".format(self.ser.baudrate))
                self.showmessage.emit("Databits: {}".format(self.ser.bytesize))
                self.showmessage.emit("Parity: {}".format(self.ser.parity))
                self.showmessage.emit("Stopbits: {}".format(self.ser.stopbits))
                self.showmessage.emit("Flowcontrol: {}".format(self.ser.xonxoff))
                self.showmessage.emit("===============================")

                self.timer.timeout.connect(lambda: self.receiveSerialData())
                self.timer.start(50)

                return True
            else:
                self.showmessage.emit("Failed to open port!")

        elif command == "Disconnect":

            self.closePort()

            if self.ser.isOpen() == False:

                return False

            else:
                self.showmessage.emit("Failed to close port!")

    def openPort(self, settingvalues):
        # This function will take the value of serial connection 
        # settings from the click event of combobox, and open 
        # specific serial port through those settings.
        
        device_selected = settingvalues[0]
        baudr_selected  = int(settingvalues[1])
        datab_selected  = DATABITS_INPUT_VALUE[settingvalues[2]]
        parity_selected = PARITY_INPUT_VALUE[settingvalues[3]]
        stopb_selected  = STOPBITS_INPUT_VALUE[settingvalues[4]]
        flowctrl_selected = FLOWCONTROL_INPUT_VALUE[settingvalues[5]]

        self.ser = serial.Serial(port= device_selected, baudrate= baudr_selected, \
                                 bytesize= datab_selected, parity= parity_selected, \
                                 stopbits= stopb_selected, xonxoff= flowctrl_selected)

        print(self.ser)
    
    def closePort(self):
        self.timer.stop()
        try:
            self.ser.close()
        except:pass

    def receiveSerialData(self):

        try:
            self.datanum = self.ser.inWaiting()
        except:
            self.closePort()

        if self.datanum > 0:
            serialdata = self.ser.read(self.datanum)
            text_received = serialdata.decode('utf-8', errors='ignore')
            self.showmessage.emit(text_received)
        
        else:pass
        
    def writeSerialData(self, commandmessage):

        try:
            messagesent = (commandmessage + "\r").encode('utf-8')
            self.ser.write(messagesent)
            self.showmessage.emit("Message sent!")

        except:
            self.showmessage.emit("Failed to send message!")

    def openLogfile(self):

        filetime = datetime.now()
        filetimestring = datetime.strftime(filetime, '%Y-%m-%d-%H%M%S')
        filename = "log_{}.txt".format(filetimestring)

        self.logfile = os.path.join(os.getcwd(), filename)

        f = open(self.logfile, mode='w')
        f.close()
        print("Logs will stored into {}. Path: {}".format(filename, self.logfile))

    
    def writelogs(self, logtext):

        logtime = datetime.now()
        timestring = datetime.strftime(logtime, '%Y-%m-%d %H:%M:%S')

        log = timestring + "\t" + logtext + "\n"

        f = open(self.logfile, mode='a')
        f.write(log)
        f.close()
