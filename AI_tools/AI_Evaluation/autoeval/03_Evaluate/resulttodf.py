import os 
import sys
import glob
import re
import pandas as pd


def patternfind(pattern, string):

    pattern = re.compile(pattern)
    target = pattern.findall(string)
    if target :
        return target[0]
    return target


def patternfindall(pattern, string):

    pattern = re.compile(pattern)
    target = pattern.findall(string)

    return target



def parse(data):

    out = list()
    tmp = list()
    finditer = False
    findtable = False

    for line in data:

        if patternfind(r"\d+k", line) and not finditer:
            iteration = patternfind(r"\d+k.*", line)
            # iteration = patternfind(r"\d+", iteration)
            iteration = iteration.replace(".txt.","")
            # iteration = int(iteration)
            finditer = True


        if finditer:
            if "SSD vs. GT" in line and not findtable:
                findtable = True

        if finditer and findtable:
            item = patternfind(r"[A-Z]\w+",line)
            if item in ["Bike", "Vehicle", "Person"]:
                precision, recall = patternfindall(r"\d+...", line)
                if tmp == []:
                    tmp.extend([iteration, item, precision, recall])
                else:
                    tmp.extend([item, precision, recall])
                if item == "Person":
                    finditer = False
                    out.append(tmp)
                    tmp = list()


    return out

def main(folder):
    # folder = sys.argv[1]
    savename = os.path.join(folder, "result.csv")

    resultlist = sorted(glob.glob("{}/*.txt".format(folder)))

    alliter = list()

    for result in resultlist:
        with open(result, "r") as f:
            data = f.readlines()

            iterresult = parse(data)
        ##add filename
        name = os.path.basename(result).replace("_Eval_draw.txt","").split("_")[4]
        iterresult[0].insert(0, name)
        alliter.extend(iterresult)
    # print(alliter)

    iterdf = pd.DataFrame(alliter, columns=["filename","iteration(k)", "item", "precision", "recall", "item", "precision", "recall", "item", "precision", "recall"])
    iterdf = iterdf.sort_values("filename")
    iterdf.to_csv(savename, index=False)
    print(iterdf)



if __name__ == '__main__':
    
    if len(sys.argv) == 2:
        folder = sys.argv[1]
        main(folder)

    else:
        sys.exit()


