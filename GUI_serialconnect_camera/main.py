# /usr/local/Caskroom/miniforge/base/envs/WorkSpace3.8
# Filename: main.py
# Author: Chester Huang

# This software is used to control the data flow through IP Camera API, 
# and its feature includes setting up serial connection, sending API 
# command and receiving data, and the storage of logs on the progress.

import sys

from PyQt5.QtWidgets import QApplication

from gui_controller import cam_Ui

def main():
    ws_ATCommand = QApplication(sys.argv)
    view = cam_Ui()
    view.show()
    sys.exit(ws_ATCommand.exec_())

if __name__ == "__main__":

    main()