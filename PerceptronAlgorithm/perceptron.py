#!/bin/python3,8
# /usr/local/Caskroom/miniforge/base/envs/py38_workspace
# Filename: perceptron.py
# Author: Chester Huang

import sklearn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.datasets import load_breast_cancer

def loadData():

    data_x, data_y = load_breast_cancer(return_X_y = True)
    print("Data_X: ", data_x, len(data_x))
    print("Data_Y: ", data_y, len(data_y))

def main():

    loadData()

if __name__ == "__main__":

    main()
