"""
Collect score from caffe training log to make a todomodel.txt, and the table of scores.
"""
import pandas as pd
import glob
import os
import re
import matplotlib.pyplot as plt
import sys
plt.switch_backend("agg")

def patternfind(pattern, string):

    pattern = re.compile(pattern)
    target = pattern.findall(string)[0]

    return target

def findscore(scorename, string):

    target = patternfind(r"{}.*".format(scorename), string)
    score = patternfind(r"\d+.\d+", target)

    return float(score)

def findtop(df, num=20):

    sortdf = df.sort_values(by=["detection_eval"], ascending=False)

    return sortdf[:num]

def findinterval(df, total=20, split=4):
    # total: the number of model we want
    # split: interval numbers

    num = total//split
    interval = round(len(df)/split)
    out = pd.DataFrame(columns=df.columns)

    for i in range(split):
        cur = df[i*interval:(i+1)*interval]
        tmp = findtop(cur, num=num)
        out = pd.concat([out,tmp])

    return out

def readlog(logpath):
    # Example
    # I0214 08:02:27.479637    26 solver.cpp:596] Snapshotting to binary proto file ./snapshot/_iter_156000.caffemodel
    # I0214 08:02:27.592260    26 sgd_solver.cpp:307] Snapshotting solver state to binary proto file ./snapshot/_iter_156000.solverstate
    # I0214 08:02:27.628062    26 solver.cpp:433] Iteration 156000, Testing net (#0)
    # I0214 08:02:27.628162    26 net.cpp:693] Ignoring source layer mbox_loss
    # I0214 08:03:40.723448    26 solver.cpp:540] class1: 0.75514
    # I0214 08:03:40.771073    26 solver.cpp:540] class2: 0.729725
    # I0214 08:03:40.849151    26 solver.cpp:540] class3: 0.724718
    # I0214 08:03:40.852414    26 solver.cpp:540] class4: 0.763603
    # I0214 08:03:40.852425    26 solver.cpp:546]     Test net output #0: detection_eval = 0.743297
    # I0214 08:03:42.208108    26 solver.cpp:243] Iteration 156000, loss = 3.10474
    # I0214 08:03:42.208149    26 solver.cpp:259]     Train net output #0: mbox_loss = 2.4497 (* 1 = 2.4497 loss)
    # I0214 08:03:42.208166    26 sgd_solver.cpp:138] Iteration 156000, lr = 2.5e-06

    print("readlog: ", logpath)

    metrix = ["class1", "class2", "class3", "class4", "detection_eval ="]

    alliter = list()
    oneiter = list()
    finditer = False

    with open(logpath, "r") as f:
        data = f.readlines()
        for line in data:

            if "solverstate" in line and not finditer:
                iteration = patternfind(r"_iter_\d+.*", line)
                iteration = patternfind(r"\d+", iteration)
                iteration = str(int(iteration)//1000)+"k"
                oneiter.append(iteration)
                finditer = True

            elif finditer:
                for want in metrix:
                    if want in line:
                        score = findscore(want, line)
                        oneiter.append(score)

                if len(oneiter) == 6:
                    alliter.append(oneiter)
                    oneiter = list()
                    finditer = False

    return alliter

def plot_eval(x, y, savename):

    fig = plt.figure(figsize=(20,6))
    plt.plot(x, y)
    plt.xticks(rotation=-45)
    plt.savefig(savename+".png")

if __name__ == '__main__':

    list_file = sys.argv[1]
    with open(list_file , "r") as f:
        loglist = f.readlines()

    todo = "todomodel.txt"
    if os.path.isfile(todo):
        os.remove(todo)

    for logpath in loglist:
        logpath = logpath.strip()
        if logpath :
            logdir = os.path.dirname(logpath)
            training_round = os.path.basename(logdir)
            backbonedir = os.path.dirname(logdir)
            backbone = os.path.basename(backbonedir)
            modeldir = os.path.join(logdir, "snapshot")

            # Parse the log to find all scores
            alliter = readlog(logpath)

            # Convert Score list to DataFrame
            iterdf = pd.DataFrame(alliter, columns=["iteration", "class1", "class2", "class3", "class4", "detection_eval"])
            savename = backbone + training_round
            iterdf.to_csv("{}.csv".format(savename), index=False)

            # Save the figure 
            # plot_eval(iterdf["iteration"], iterdf["detection_eval"], savename)

            # Find Top 5 through diffrent interval
            selectdf = findinterval(iterdf)
            iterlist = selectdf['iteration'].tolist()
            selectdf.to_csv("{}_select.csv".format(savename), index=False)

            # Create todomodel.txt
            with open(todo , "a") as f2:
                for iteration in iterlist:
                    iteration = iteration.replace("k", "000")
                    modelname = "_iter_{}.caffemodel".format(iteration)
                    modelpath = os.path.join(modeldir, modelname)
                    f2.write(modelpath)
                    f2.write("\n")



