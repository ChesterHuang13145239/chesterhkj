import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from scipy.sparse import data
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm       import SVC
from sklearn.tree      import DecisionTreeClassifier
from sklearn.ensemble  import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB

C1_TRAIN_X = os.path.join(os.getcwd(), "Datasets", "Fakepoints_datasets", "c1_train.txt")
C2_TRAIN_X = os.path.join(os.getcwd(), "Datasets", "Fakepoints_datasets", "c2_train.txt")
C1_TEST_X  = os.path.join(os.getcwd(), "Datasets", "Fakepoints_datasets", "c1_test.txt")
C2_TEST_X  = os.path.join(os.getcwd(), "Datasets", "Fakepoints_datasets", "c2_test.txt")

def readData(filename, y):

    with open(filename, "r") as f:
        lines     = f.readlines()
        rows      = len(lines)
        datamat_x = np.zeros((rows, 2))
        datamat_y = np.zeros(rows, dtype = int)
 
        for i in range(rows):
            line = lines[i].rstrip("\n").split(" ")
            x1   = float(line[0])
            x2   = float(line[1])
            datamat_x[i] = [x1, x2]
            datamat_y[i] = int(y)

    return datamat_x, datamat_y

def collectDataAll(datamat_x1, datamat_x2, datamat_y1, datamat_y2):

    datamat_x1x2 = np.concatenate([datamat_x1, datamat_x2])
    datamat_y1y2 = np.concatenate([datamat_y1, datamat_y2])

    return datamat_x1x2, datamat_y1y2

def findMaxmin(c1_train_x, c2_train_x, c1_test_x, c2_test_x):

    data_x  = np.concatenate([c1_train_x, c2_train_x, c1_test_x, c2_test_x])
#    print("data_x: ", data_x)
    dtx_max = data_x[:, 0].max() + .5
    dtx_min = data_x[:, 0].min() - .5
    dty_max = data_x[:, 1].max() + .5
    dty_min = data_x[:, 1].min() - .5

    return dtx_max, dtx_min, dty_max, dty_min

if __name__ == "__main__":

    h = .02
    names = ["Nearest Neighbors", "Poly SVM", "RBF SVM",
             "Decision Tree",
             "Random Forest", "AdaBoost", "Naive Bayes"]

    Classifier = [KNeighborsClassifier(3, weights = "distance"),
        SVC(kernel = "poly", C = 1, degree = 2),
        SVC(gamma = "auto", C = 1),
        DecisionTreeClassifier(ccp_alpha=0.0, class_weight={0: 1, 1: 10}, criterion='gini',
                       max_depth=15, max_features=2, max_leaf_nodes=None,
                       min_impurity_decrease=0.0001, min_impurity_split=None,
                       min_samples_leaf=1, min_samples_split=2,
                       min_weight_fraction_leaf=0.0001, presort='deprecated',
                       random_state=1, splitter='best'),
        RandomForestClassifier(bootstrap=True, ccp_alpha=0.0, class_weight=None,
                       criterion='gini', max_depth=15, max_features=1,
                       max_leaf_nodes=None, max_samples=None,
                       min_impurity_decrease=0.0, min_impurity_split=None,
                       min_samples_leaf=1, min_samples_split=2,
                       min_weight_fraction_leaf=0.0, n_estimators=50,
                       n_jobs=None, oob_score=False, random_state=None,
                       verbose=0, warm_start=False),
        AdaBoostClassifier(algorithm='SAMME.R', n_estimators=15, learning_rate=0.01, random_state=None),
        GaussianNB()]

#    print(Classifier)

    fig = plt.figure(figsize= (27, 9))
    i   = 1

    c1_train_x, c1_train_y = readData(C1_TRAIN_X, 0) 
    c2_train_x, c2_train_y = readData(C2_TRAIN_X, 1)
    c1_test_x, c1_test_y   = readData(C1_TEST_X, 0)
    c2_test_x, c2_test_y   = readData(C2_TEST_X, 1)
    print("c1_train_x: ", c1_train_x, type(c1_train_x), len(c1_train_x))
#    print("c1_train_y: ", c1_train_y, type(c1_train_y), len(c1_train_y))
#    print("c2_train_x: ", c2_train_x, type(c2_train_x), len(c2_train_x))
#    print("c2_train_y: ", c2_train_y, type(c2_train_y), len(c2_train_y))

    c1c2_train_x, c1c2_train_y = collectDataAll(c1_train_x, c2_train_x, c1_train_y, c2_train_y)
    c1c2_test_x, c1c2_test_y   = collectDataAll(c1_test_x, c2_test_x, c1_test_y, c2_test_y)

    dtx_max, dtx_min, dty_max, dty_min = findMaxmin(c1_train_x, c2_train_x, c1_test_x, c2_test_x)
    xx, yy = np.meshgrid(np.arange(dtx_min, dtx_max, h),
                         np.arange(dty_min, dty_max, h))
#    print("dtx_max: ", dtx_max, type(dtx_max))
#    print("dtx_min: ", dtx_min, type(dtx_min))
#    print("dty_max: ", dtx_max, type(dtx_max))
#    print("dty_min: ", dtx_min, type(dtx_min))
    cm = plt.cm.RdBu
    cm_bright = ListedColormap(['#FF0000', '#0000FF'])

    ax = plt.subplot(1, len(Classifier) + 1, i)
    ax.scatter(c1c2_train_x[:, 0], c1c2_train_x[:, 1], c = c1c2_train_y, cmap = cm_bright, edgecolors = "k")
#    ax.scatter(c2_train_x[:, 0], c2_train_x[:, 1], color = "#FF0000", edgecolors = "k")
    ax.scatter(c1c2_test_x[:, 0], c1c2_test_x[:, 1], c = c1c2_test_y, cmap = cm_bright, alpha = 0.6, edgecolors = "k")
#    ax.scatter(c2_test_x[:, 0], c2_test_x[:, 1], color = "#FF0000", alpha = 0.6, edgecolors = "k")

    ax.set_xticks(())
    ax.set_yticks(())
    ax.set_title("Input Data")
    i += 1

    for name, clf in zip(names, Classifier):
        ax = plt.subplot(1, len(Classifier) + 1, i)
        clf.fit(c1c2_train_x, c1c2_train_y)
        score = clf.score(c1c2_test_x, c1c2_test_y)
        print("{} score: ".format(name), score)

        if hasattr(clf, "decision_function"):
            Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
        else:
            Z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]
            print("Z:", Z)
        Z = Z.reshape(xx.shape)
        print("Z_reshape:", Z)
        ax.contourf(xx, yy, Z, cmap=cm, alpha=.8)

        # Plot the training points
#        ax.scatter(c1c2_train_x[:, 0], c1c2_train_x[:, 1], c = c1c2_train_y, cmap = cm_bright, edgecolors = "k")
#        ax.scatter(c2_train_x[:, 0], c2_train_x[:, 1], color = "#FF0000", edgecolors = "k")
        # Plot the testing points
        ax.scatter(c1c2_test_x[:, 0], c1c2_test_x[:, 1], c = c1c2_test_y, cmap = cm_bright, alpha = 0.6, edgecolors = "k")
#        ax.scatter(c2_test_x[:, 0], c2_test_x[:, 1], color = "#FF0000", alpha = 0.6, edgecolors = "k")

        ax.set_xlim(xx.min(), xx.max())
        ax.set_ylim(yy.min(), yy.max())
        ax.set_xticks(())
        ax.set_yticks(())
#        if ds_cnt == 0:
        ax.set_title(name)
        ax.text(xx.max() - .3, yy.min() + .3, ('%.2f' % score).lstrip('0'),
                size=15, horizontalalignment='right')
        i += 1

plt.tight_layout()
plt.show()
