import pandas as pd
import re
import sys
import os
import glob

def patternfind(pattern, string):

    pattern = re.compile(pattern)
    target = pattern.findall(string)
    if target:
        target = target[0]

    return target

def findscore(scorename, string):

    score = patternfind(r"\d+.\d+", string)
    return float(score)*0.01

def parse(data):

    metrix = ["Bike", "Person", "Vehicle", "mAP"]

    alliter = list()
    oneiter = list()
    finditer = False
    for line in data:
        if patternfind(r"\d+k", line) and not finditer:
            iteration = patternfind(r"\d+k.*", line)
            iteration = iteration.replace(".txt.","")
            oneiter.append(iteration)
            finditer = True

        if finditer:
            for want in metrix:
                if want in line:
                    score = findscore(want, line)
                    score = round(score, 4)
                    oneiter.append(score)
            if len(oneiter)==5:
                alliter.append(oneiter)
                oneiter = list()
                finditer = False
    return alliter

def main(folder):
    # folder = sys.argv[1]
    savename = os.path.join(folder, "mAP.csv")
    alliter = list()
    # print(folder)

    mAPlist = sorted(glob.glob("{}/*.txt".format(folder)))

    for mAPfile in mAPlist:
        

        with open(mAPfile, "r") as f:
            data = f.readlines()
            iterresult = parse(data)
        
        name = os.path.basename(mAPfile).replace("_Eval_draw.txt","").split("_")[4]
        iterresult[0].insert(0, name)
        alliter.extend(iterresult)

    iterdf = pd.DataFrame(alliter, columns=["filename", "iteration(k)", "Bike", "Person", "Vehicle", "mAP"])
    iterdf = iterdf.sort_values("filename")
    iterdf = iterdf[["filename", "iteration(k)", "Bike", "Vehicle", "Person", "mAP"]]
    iterdf.to_csv(savename, index=False)
    print(iterdf)


if __name__ == '__main__':

    if len(sys.argv) == 2:
        folder = sys.argv[1]
        main(folder)
    else:
        sys.exit()