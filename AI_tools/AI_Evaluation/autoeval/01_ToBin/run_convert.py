import os, sys
import json

with open("convert_config.json", "r") as f:
    config = json.load(f)

# img_path   = sys.argv[1]
# deploy     = sys.argv[2]
# caffemodel = sys.argv[3]
# outname    = sys.argv[4]

img_path = config["img_path"]
deploy = config["deploy"]
caffemodel = config["caffemodel"]
model_w = config["Input Width"]
model_h = config["Input Height"]

outname = "{}_{}-{}-{}".format(config["Network Name"], 
                               config["backbone"], 
                               config["training_round"], 
                               config["training_iter"])

# if len(sys.argv) < 5:
#     print("run with arg {img_path} {deploy} {caffemodel} {cavalry_name}")
#     exit()

# model_w = 640
# model_h = 360

amba_vas = outname + ".vas"
amba_bin = "cavalry_" + outname + ".bin"

def printinfo(string):
    print("\n\033[32m {} \033[0m".format(string))

cmd1 = "ssd_prior_box_handling.py -p {} -c {} -op out/remove_prior_box.prototxt -opb out/priorbox_fp32.bin".format(deploy, caffemodel)
#ssd_prior_box_handling.py -p deploy.prototxt -c _iter_86000.caffemodel -op out/remove_prior_box.prototxt -opb out/priorbox_fp32.bin 

cmd2 = "gen_image_list.py -f img -o out/img_dra_list.txt -ns -e jpg -c 0 -d 0,0 -r {},{} -bf out/dra_bin/ -bo out/dra_list.txt".format(model_h, model_w)
#gen_image_list.py -f img -o out/img_dra_list.txt -ns -e jpg -c 0 -d 0,0 -r 360,640 -bf out/dra_bin/ -bo out/dra_list.txt 

cmd3 = "caffeparser.py -p out/remove_prior_box.prototxt -m {} -i out/dra_list.txt -o {} -of out/out_parser/ -iq -idf 0,0,0,0 -c act-force-fx8,coeff-force-fx8 -odst \"o:mbox_loc|odf:fp32\" -odst \"o:mbox_conf_flatten|odf:fp32\" -dinf cerr".format(caffemodel, outname)
#caffeparser.py -p out/remove_prior_box.prototxt -m _iter_86000.caffemodel -i out/dra_list.txt -o cavalry_ssd_v6.1.1_640x360_mobilenetv1.2-trial-1a-86k -of out/out_parser/ -iq -idf 0,0,0,0   -c act-force-fx8,coeff-force-fx8  -odst o:mbox_loc|odf:fp32 -odst o:mbox_conf_flatten|odf:fp32 -dinf cerr

cmd4 = "vas -auto -show-progress {}".format(amba_vas)
#vas -auto -show-progress cavalry_ssd_v6.1.1_640x360_mobilenetv1.2-trial-1a-86k.vas 

cmd5 = "cavalry_gen -d out/out_parser/vas_output/ -f out/{}".format(amba_bin)
#cavalry_gen -d out/out_parser/vas_output/ -f cavalry_cavalry_ssd_v6.1.1_640x360_mobilenetv1.2-trial-1a-86k.bin 

if os.path.isdir("out"):
    os.system("rm -rf out")
os.system("mkdir -p -m 755 out")

printinfo(cmd1)
os.system(cmd1)

printinfo(cmd2)
os.system(cmd2)

printinfo(cmd3)
os.system(cmd3)

os.chdir("out/out_parser")
printinfo(cmd4)
os.system(cmd4)

os.chdir("../../")
printinfo(cmd5)
os.system(cmd5)

