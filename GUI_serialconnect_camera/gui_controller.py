import os
import PyQt5

from PyQt5 import (QtCore, 
                   QtWidgets
                   )
from PyQt5.QtCore import (Qt,
                          QTimer
                          )
from PyQt5.QtWidgets import (QMainWindow,
                             QSplitter,
                             QHBoxLayout,
                             QVBoxLayout,
                             QGridLayout,
                             QTabWidget,
                             QLabel,
                             QLineEdit,
                             QWidget,
                             QGroupBox,
                             QPushButton,
                             QComboBox,
                             QPlainTextEdit,
                            )

from config import (SystemSetting, \
                    POC_MFG_API, \
                    SERIAL_CONNECTION_SETTINGS, \
                    SERIAL_CONNECTION_DEFAULT_SETTINGS
                    )

from workthread import Worker

class cam_Ui(QMainWindow):
    # The view of GUI_serialconnection_camera.

    def __init__(self, parent=None):

        # View Initializer.
        super(QMainWindow, self).__init__()
        self.portsetting = SystemSetting()
        self.worker = Worker()
        self.tabwin = QTabWidget()
        self.insertwin = QWidget()
        self.messagewin = QWidget()
        self.splittop  = QSplitter(Qt.Horizontal)
        self.splitmain = QSplitter(Qt.Vertical)
        self.timer = QTimer()
        

        self.initUI()
    
    def initUI(self):
        self.worker.showmessage.connect(self.inputMessage)
        self.worker.start()
        self.worker.openLogfile()
        # Set the title and minimized size of Main Window.
        self.setWindowTitle("Primax MFG AT Command Utility V1.0")
        self.setMinimumSize(1260, 720)

        # Set central widget of Main Window displayed with 
        # tabs and the status bar. 
        self.createInsertWidget()
        self.createCommandTab()
        self.createMessageWidget()
        self.splittop.addWidget(self.insertwin)
        self.splittop.addWidget(self.tabwin)
        self.splittop.setSizes([240, 960])
        self.splitmain.addWidget(self.splittop)
        self.splitmain.addWidget(self.messagewin)
        self.splitmain.setFocus()
        self.setCentralWidget(self.splitmain)


    def createInsertWidget(self):
        # This function is used to create and display widgets which
        # can input serial connection settings, including Port,
        # Baud rate, Data bits, Stop bits and Parity.

        self.usb_portdict  = self.worker.portdict
        self.settingwidgets = QGroupBox("System Setting", self)
        self.settingwidgets.setStyleSheet("QGroupBox{color:rgb(0,125,125);\
                                          font-size:18px;\
                                          font-weight:bold}")
        self.comboboxlist = []
        settingwidgets_layout = QVBoxLayout()
        insertwidgets_layout = QGridLayout()
        insertwidgets_layout = self.displayUSBPortWidget(insertwidgets_layout, self.usb_portdict)
        insertwidgets_layout = self.displaySettingWidget(insertwidgets_layout)

        self.settingwidgets.setLayout(insertwidgets_layout)
        settingwidgets_layout.addWidget(self.settingwidgets)
        self.insertwin.setLayout(settingwidgets_layout)
    
    def createCommandTab(self):
        # The function is used to display tabs for inserting
        # parameters and sending messages to the machine 
        # though USB port.
        self.commanddict = {}
        self.commandtab = QGroupBox("AT Command Tab", self)
        self.commandtab.setStyleSheet("QGroupBox{color:rgb(0,125,125);\
                                       font-size:18px;\
                                       font-weight:bold}")
        commandtab_layout = QVBoxLayout()
        self.tabs = QTabWidget()
        tabs_layout = QHBoxLayout()
        # Set the display of different tabs.
        for tabname, APIs in POC_MFG_API .items():
            self.createTabWidget(tabname, APIs)

        # Set the display of whole tabs, so that it could
        # be called by class cam_Ui and displayed on top splitter
        # on Main Window.
        tabs_layout.addWidget(self.tabs)
        self.commandtab.setLayout(tabs_layout)
        commandtab_layout.addWidget(self.commandtab)
        self.tabwin.setLayout(commandtab_layout)

    def createMessageWidget(self):
        # The class is used to create widget showing messages 
        # sent from and sent to the machine though USB port.

        self.messagewidget = QGroupBox("Log Window", self)
        self.messagewidget.setStyleSheet("QGroupBox{color:rgb(0,125,125);\
                                                    font-weight:bold;\
                                                    font-size:18px}")

        messagewidget_layout = QVBoxLayout()
        self.messagebox = QPlainTextEdit()
        self.messagebox.setReadOnly(True)
        self.messagebox.setMinimumSize(800, 240)
        self.clearbutton = QPushButton("Clear")
        self.clearbutton.clicked.connect(self.clearMessage)
        messagebox_layout = QHBoxLayout()
        messagebox_layout.addWidget(self.messagebox)
        messagebox_layout.addWidget(self.clearbutton)
        self.messagewidget.setLayout(messagebox_layout)
        messagewidget_layout.addWidget(self.messagewidget)

        self.messagewin.setLayout(messagewidget_layout)

    def displayUSBPortWidget(self, insertwidgets_layout, usb_portdict):
        # Set display of combobox used to choose USB port.

        # usb_ports will collect though serial module, and be displayed as
        # the first row of System Setting group.
        usb_ports = usb_portdict.keys()
        title = QLabel("Ports")
        title.setStyleSheet("QLabel{font-family: Arial;\
                                    font-size:14px;\
                                    font-weight:bold}")

        port_combobox = self.createComboBox(usb_ports, 0)
        port_combobox.setFixedWidth(150)
        
        insertwidgets_layout.addWidget(title, 0, 0, Qt.AlignLeft)
        insertwidgets_layout.addWidget(port_combobox, 0, 1, Qt.AlignLeft)
        
        return insertwidgets_layout
    
    def displaySettingWidget(self, insertwidgets_layout): 
        # Set display of comobobox used to set up serial 
        # connection setteings and pushbuttons used to connect
        # and disconnect to device.

        # Setting list will collect titles of System Setting group 
        # like Baudrate and Databits into a list.
        setting_list  = list(SERIAL_CONNECTION_SETTINGS.keys())
        settingrows = len(setting_list)

        # Set settingtitle and combobox used to input serial 
        # connection settings.
        for settingkey, settingvalue in SERIAL_CONNECTION_SETTINGS.items():
            # Variable row represents the number of row which settingtitle 
            # and combobox collecting settingvalue will be displayed. Because 
            # first row has been used to display title and combobox for choosing
            # port, so row start from 1.
            row  = setting_list.index(settingkey) + 1
            settingtitle = QLabel(settingkey)
            settingtitle.setStyleSheet("QLabel{font-family: Arial;\
                                        font-size:14px;\
                                        font-weight:bold}")
            settingcombobox = self.createComboBox(settingvalue, row)
            settingcombobox.setFixedWidth(150)
            insertwidgets_layout.addWidget(settingtitle, row, 0, Qt.AlignLeft)
            insertwidgets_layout.addWidget(settingcombobox, row, 1, Qt.AlignLeft)

        # Set pushbuttons used to connect and discoonect to device.
        self.connectbutton = QPushButton("Connect")
        self.connectbutton.clicked.connect(lambda: self.manageConnection(command="Connect"))
        self.disconnectbutton = QPushButton("Disconnect")
        self.disconnectbutton.clicked.connect(lambda: self.manageConnection(command = "Disconnect"))
        insertwidgets_layout.addWidget(self.connectbutton, settingrows + 1, 0, Qt.AlignLeft)
        insertwidgets_layout.addWidget(self.disconnectbutton, settingrows + 1, 1, Qt.AlignLeft)

        # Set labels used to show the result of serial connection setting.
        connectionlabel = QLabel("Connection Status")
        connectionlabel.setStyleSheet("QLabel{font-family: Arial;\
                                       font-size:14px;\
                                       font-weight:bold}")
        self.connectionmessage = QLabel("Wait for setup...")
        insertwidgets_layout.addWidget(connectionlabel, settingrows + 2, 0, Qt.AlignLeft)
        insertwidgets_layout.addWidget(self.connectionmessage, settingrows + 2, 1, Qt.AlignLeft)

        return insertwidgets_layout


    def createComboBox(self, items, itemnum):
        # This function in used to put items into combobox, 
        # and create the click event of combobox.

        combobox = QComboBox()
        combobox.addItems(items)
        # Display the default serial connection settingvalue on GUI.
        combobox.setCurrentText(SERIAL_CONNECTION_DEFAULT_SETTINGS[itemnum - 1])
        self.comboboxlist.append(combobox)
        self.comboboxlist[itemnum].activated[str].connect(lambda: self.findCurrentText(itemnum))

        return combobox

    def findCurrentText(self, itemnum):
        # This function is used to let the system find the 
        # current text of specific combobox and collect that 
        # text in settingvalue of class SystemSetting.

        clickcomb = self.comboboxlist[itemnum]
        currenttext = clickcomb.currentText()
        self.portsetting.collectSettingValue(itemnum, currenttext)

    def manageConnection(self, command):
        # This function is used to conetrol the click event 
        # of connectbutton and disconnectbutton, which includes 
        # setting values used to open or close port for serial 
        # connection and changing the connection status showed 
        # on GUI.

        connectionstatus = self.worker.controlPortConnection(self.portsetting.settingvalues, command)
        self.determineConnectionMessage(connectionstatus)

    def determineConnectionMessage(self, connectionstatus):
        # This function is used to determine the message of 
        # connection status by settingvalues of class SystemSetting.

        if connectionstatus == True:
            self.worker.showmessage.emit("Serial Connection start!")
            self.worker.showmessage.emit("===============================")
            self.connectionmessage.setText("Connected")
            self.connectionmessage.setStyleSheet("QLabel{font-family: Arial;\
                                                  font-size:14px;\
                                                  color: green}")
        
        elif connectionstatus == False:
            self.worker.showmessage.emit("Serial Connection end!")
            self.worker.showmessage.emit("===============================")
            self.connectionmessage.setText("Disconnected")
            self.connectionmessage.setStyleSheet("QLabel{font-family: Arial;\
                                                  font-size:14px;\
                                                  color: red}")

    def createTabWidget(self, tabname, APIs):
        tab = QWidget()
        self.tabs.addTab(tab, tabname)
        # Use QGridLayout to put each widget at speicfic position
        # for better display when Main Window is resized.
        tab_layout = QGridLayout()
        tab_layout = self.displayTopics(tab_layout)
        tab_layout = self.displayController(tab_layout, APIs)
        tab.setLayout(tab_layout)

    def displayTopics(self, tab_layout):
        # Set topics of different columns which are displayed 
        # at the first line of each tab.
        topics = ["Test\nItem", "Field0\nHeader", "Field1\nOpcode", \
                  "Field2\nParam1", "Field3\nParam2",\
                  "Field4\nParam3", "Field5\nReserved",\
                  "Field6\nReserved", "Field7\nChecksum"]
        
        for i in range(len(topics)):
            topic = QLabel(topics[i])
            topic.setWordWrap(True)
            topic.setAlignment(Qt.AlignCenter)
            topic.setStyleSheet("QLabel{color:rgb(0,125,125);\
                                        font-family: Arial;\
                                        font-size:14px;\
                                        font-weight:bold}")
            tab_layout.addWidget(topic, 0, i, Qt.AlignCenter)
        
        return tab_layout

    def displayController(self, tab_layout, APIs):
        # Set display of items, fields(headers, params) and button 
        # used for sending messages.
        # TODO: Make lines of items used to send message to be listed
        #       in the same place at the same line
        api = list(APIs.keys())        

        count = 1
        while count <= len(APIs):
            item = QLabel(api[count - 1])
            item.setAlignment(Qt.AlignLeft)
            tab_layout.addWidget(item, count, 0, Qt.AlignTop)
            cmd = APIs[api[count - 1]]

            for i in range(1, len(cmd) + 1):
                # cmd[0] = command ID which is used to identify command
                # sent to the device, so the whole display of command tab 
                # start from index = 1, and index will represent the real 
                # place of cmd on the grid because (count, 0) = item.
                cmd_id = cmd[0]
                if i < 3:
                    # Varaible header represents "Field0 Header" and
                    # "Field1 Opcode".
                    header = QLabel(cmd[i])
                    self.commanddict = self.collectwidgets(cmd_id, header)
                    header.setAlignment(Qt.AlignCenter)
                    tab_layout.addWidget(header, count, i, 1, 1, Qt.AlignTop)
                
                elif 2 <= i <= (len(cmd) - 1):
                    # Variable param represents "Field2 Param1", 
                    # "Field3 Param2", "Field4 Param3" and reserved 
                    # parameters.
                    param = QLineEdit("")
                    self.commanddict = self.collectwidgets(cmd_id, param)
                    param.setFixedSize(80, 20)
                    param.setAlignment(Qt.AlignCenter)
                    if cmd[i] == "Null":
                        # Set the display of Reseved parameter, make it uneditable
                        # and under grey background-color.
                        param.setReadOnly(True)
                        param.setFixedSize(80, 20)
                        param.setStyleSheet("QLineEdit{background-color:rgb(187,191,187)}")
                    else:
                        param.setPlaceholderText(cmd[i])

                    tab_layout.addWidget(param, count, i, 1, 1, Qt.AlignTop)
            
                elif i == len(cmd):
                    # Variable button represents button used to 
                    # send message to Winer Soldier by click.

                    button = QPushButton("Send")
                    button.clicked.connect(lambda signal, id =cmd_id: self.createCommand(id))
                    tab_layout.addWidget(button, count, i + 1, 1, 1, Qt.AlignTop)

            count += 1
        
        return tab_layout

    def collectwidgets(self, cmd_id, widget):
        # This function is used to collect widgets under 
        # different commands, and store all widgets into
        # commanddict for further usage of creating
        # command message.

        widgetlist = []
        cmd_ids = self.commanddict.keys()

        if cmd_id not in cmd_ids:
            widgetlist.append(widget)
            self.commanddict[cmd_id] = widgetlist

        else:
            widgetlist = self.commanddict[cmd_id]
            widgetlist.append(widget)
            self.commanddict[cmd_id] = widgetlist

        return self.commanddict

    def createCommand(self, cmd_id):
        # This function is used to collect and create
        # command messages for the machine operation.

        commandtext = []
        commandlist = self.commanddict[cmd_id]
        # The header of POC_MFG_API , which is equal to "XM".
        header = commandlist[0].text()
        # The opcode of POC_MFG_API , which 
        # is equal to item of API, like "HELP" or "WIFI".
        opcode = commandlist[1].text()
        # The parameters of API, default = "", and the value
        # of parameters will change by input text.
        param1 = commandlist[2].text()
        param2 = commandlist[3].text()
        param3 = commandlist[4].text()

        command = header + "=" + opcode
        commandtext = [command, param1, param2, param3]

        for i in range(4):
            if "" in commandtext:
                num = commandtext.index("")
                commandtext.remove(commandtext[num])
        # This line is used to create string of command 
        # message under format of the machine.
        commandmessage = ",".join(commandtext)
        self.worker.showmessage.emit("Command: {}".format(commandmessage))
        self.worker.writeSerialData(commandmessage)

    def inputMessage(self, message):

        self.messagebox.appendPlainText(message)
        self.worker.writelogs(message)

    def clearMessage(self):

        self.messagebox.clear()
