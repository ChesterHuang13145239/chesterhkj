# Decision Tree

## A. 簡介
Decision Tree 是處理分類問題的一種方法，過程中重複透過數據的屬性或問題選出節點，對數據進行分割產生分支，藉以完成決策，整體結構呈樹狀。由於其結構特性，Decision Tree 具有「解釋性強」的特點。

## B. 演算方式
在樹狀結構中，主要有根節點、父節點、子節點和樹葉節點等節點種類。Decision Tree 演算法的運行即是從根節點開始，依據特徵將dataset 分割到不同節點中，若該節點內的資料皆屬於同一類，則該節點為樹葉節點，將不再繼續分配；反之若存在不同類型的資料，則該節點為子節點及下一輪分割的父節點，理論上會持續分割到所有節點皆為樹葉節點為止。至於如何分割，則與資訊增益(Information gain)和不純度(Impurity measure)之衡量有關。

### 1. 資訊增益(Information gain, IG)
Decision Tree 的運作仰賴對特徵的選擇，從而進行dataset 的分割，而資訊增益則是特徵選擇的判斷依據，標準為：若該特徵能取得最大的資訊增益，則依據該特徵進行分割。資訊增益的公式如下方所示。

![Information_Gain_Formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_InformationGain_1.png?inline=false)

![Information_Gain_Formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_InformationGain_2.png?inline=false)

由上可知，資訊增益即父節點之不純度(I)，和子節點不純度加總的差值，因此在父節點不純度固定的情況下，若挑選出的特徵能讓子節點的不純度加總越小，資訊增益即越大。另外，由於實務上常用二元決策樹的形式進行，因此資訊增益的計算也可用下方公式表示。

![Information_Gain_Formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_InformationGain_3.png?inline=false)

![Information_Gain_Formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_InformationGain_4.png?inline=false)

### 2. 不純度(Impurity measure, I)
在進行Decision Tree 演算上，計算不純度的方式主要有熵(Entrophy) 、吉尼不純度(Gini Impurity) 和分配錯誤率(Misclassification Rate) 三種，其中又以熵和吉尼不純度為主流，且兩者有很大機率得出相同結論。三者公式如下所示。p(i|t)指「i種類樣本之於某t節點內樣本總數的比例」。

#### a. 熵(Entrophy)
![Entrophy_Formula](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_Entrophy_1.png?inline=false)

由上述公式可知，若是一節點在二元決策樹下為樹葉節點，則p(i|t) = 0 or 1 ，此時的熵最小(= 0) ；反之，若一節點底下的樣本平均的分屬兩種樣本，則此時不論何種樣本的p(i|t)皆相同(= 0.5) ，此時熵最大(= 1) 。因此，熵的大小也可以作為樣本「不確定性」的表現，熵的值越大，樣本的不確定性越高，反之亦然。

#### b. 吉尼不純度(Gini Impurity)
![Gini_Impurity_Formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_GiniImpurity_1.png?inline=false)

由上述公式可知，若節點為二元決策樹的樹葉節點，p(i|t)的加總最大(= 1) ，吉尼不純度最小(= 0) 。反之，如果若一節點底下的樣本平均的分屬兩種樣本，則p(i|t) 平方的總和最小(= 0.5) ，吉尼不純度最大(= 0.5) ，如下方等式所示。

![Gini_Impurity_Formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_GiniImpurity_2.png?inline=false)

綜合上述結果，吉尼不純度和熵具有相同的特性，即和樣本的「不確定性」成正比。

#### c. 分配錯誤率(Misclassification Rate)
![Misclassification_Rate_Formula](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/example_Misclassification_1.png?inline=false)

### 3. Example: 以資訊增益和不純度檢視特徵選擇的結果
假設有一組內含兩種不同樣本的動物特徵數據，組成為50個動物A，和50個動物B，並使用Decision Tree 來進行分類。以下將針對選擇不同特徵後的分類結果，說明Decision Tree 的計算，以及資訊增益和不同不純度計算方式的差異。

#### a. 特徵完整區分兩種樣本的資訊增益
假設在執行Decision Tree Classifier 時有一特徵，能夠完美區分動物A 和動物B。以下將示範以三種不純度分別計算資訊增益之結果。

![ExampleOne_result](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleOne_result.png?inline=false)

##### (1) 熵
![ExampleOne_CalculateIG_Entrophy](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleOne_CalculateIG_Entrophy.png?inline=false)

##### (2) 吉尼不純度
![ExampleOne_CalculateIG_GiniImpurity](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleOne_CalculateIG_GiniImpurity.png?inline=false)

##### (3) 分配錯誤率
![ExampleOne_CalculateIG_MisclassificationRate](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleOne_CalculateIG_MisclassificationRate.png?inline=false)

前面介紹不同不純度時，曾經分析各種不純度的最大值，以及不純度大小和不確定性之間的關係。根據資訊增益的公式，如果子節點的不純度加總越小，該特徵的資訊增益就會越大，換言之如果樣本的不確定性越小，資訊增益自然越大。上述案例中，該特徵能夠完美的區分資料中的兩種樣本，因此子節點的不確定性最小(= 0) ，所以此時的資訊增益將會是最大值。

#### b. 不同特徵之分類結果比較
假設在執行Decision Tree Classifier 時，有兩特徵可供選擇。下方左圖為依據特徵一所產生的分類結果，下方右圖則為依據特徵二所產生的分類結果。此時可利用不同的不純度計算資訊增益，看看對於特徵選擇的結果是否相同。

![ExampleTwo_result](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_result.png?inline=false)

##### (1) 熵
###### 特徵一.
![ExampleTwo_CalculateIG_Entrophy](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_CalculateIG_Entrophy_1.png?inline=false)

###### 特徵二.
![ExampleTwo_CalculateIG_Entrophy](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_CalculateIG_Entrophy_2.png?inline=false)

##### (2) 吉尼不純度
###### 特徵一.
![ExampleTwo_CalculateIG_GiniImpurity](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_CalculateIG_GiniImpurity_1.png?inline=false)

###### 特徵二.
![ExampleTwo_CalculateIG_GiniImpurity](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_CalculateIG_GiniImpurity_2.png?inline=false)

##### (3) 分配錯誤率
###### 特徵一.
![ExampleTwo_CalculateIG_MisclassificationRate](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_CalculateIG_MisclassificationRate_1.png)

###### 特徵二.
![ExampleTwo_CalculateIG_MisclassificationRate](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/ExampleTwo_CalculateIG_MisclassificationRate_2.png?inline=false)

由前面的推導過程可知，不論利用何種不純度來進行資訊增益的計算，特徵二所取得之資訊增益皆大於特徵一，因此Decision Tree 在進行特徵選擇時，會選擇特徵二來進行分類工作。

## C. 剪枝(Prunning)
Decision Tree 的運算需要藉由特徵選擇來產生分支，但任由Decision Tree 對資料不斷進行分類，則有很大的可能造成整個Decision Tree 分支過多，出現過分擬合(overfitting) 的問題，這也是此分類演算法主要的缺點所在。而此時就必須對Decision Tree 進行剪枝(Prunning) 。

剪枝(Prunning) 顧名思義就是透過一些操作，減少Decision Tree 的分支，從而解決過分擬合的問題。依據進行剪枝的時間點不同，可分為預剪枝(Pre-prunning) 和後剪枝(Post-prunning) 兩種。

### 1. 預剪枝(Pre-prunning)
預剪枝指，在運行Decision Tree Classifier 之前，先對其設下一些限制，讓分類工作提前終止。Decision Tree Classifier 有很大可能發生過度擬合問題的原因，即是因為沒有設下終止條件的話，其將運行到所有節點都是葉節點為止，因此預剪枝即是透過對最大深度、葉節點的最小樣本數等作出限制，讓Decision Tree Classifier 在造成過度擬合問題前停下，避免問題發生。

下方是sklearn 中DecisionTreeClassifier 的相關參數，當中即包含一些根據常用的預剪枝條件所設計的參數，接下來將簡單這些參數的意涵。若像參考sklearn 中關於參數的介紹，請點[此處](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier)。
>class sklearn.tree.DecisionTreeClassifier(*, criterion='gini', splitter='best', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features=None, random_state=None, max_leaf_nodes=None, min_impurity_decrease=0.0, class_weight=None, ccp_alpha=0.0)

#### a. max_depth
此參數是用來限制Decision Tree 的最大深度，深度也可以看作是Decision Tree 的層數。當Decision Tree 的層數超過其最大深度限制時，分類工作即終止。需要注意的是，由於預剪枝是在執行Decision Tree Classifier 前先行設定的，對於使用者而言，真正的最大深度未知，因此若限制大於該Decision Tree 的真正最大深度，這項預剪枝的參數將失去其效果。

#### b. min_samples_leaf
此參數是限制形成樹葉節點的最小樣本數量，預設值為1 。樹葉節點的形成條件，是該節點內不存在任何的子節點，因此樹葉節點內的樣本數量應該大於或等於1 ，也是min_samples_leaf 的預設值為1 的原因。假如設定min_samples_leaf = 10 ，則Decision Tree 在分割節點時，會確保不同分支的葉節點樣本數皆大於10 ，才會進行分割；反之，若無法讓分割後不同分支的葉節點樣本數皆大於10 ，則不進行分割。

#### c. min_samples_split
此參數是限制進行分割的最小樣本數，預設值為2 ，意即在預設情況下，子節點的樣本數必須大於或等於2 ，才能作為父節點被分割。在進行預剪枝時，可以透過調高此項參數的值，讓樣本數小於該值的的節點停止分割成為葉節點，藉此提前停止該節點的分類工作。

#### d. max_features
此參數是限制Decision Tree Classifier 所選擇特徵的最大數量。特徵選擇是Decision Tree 持續運行的依據，前面提到Decision Tree 的過度擬合問題通常發生在分支過多的情況下，而分支越多也代表Decision Tree 所選用的特徵越多，因此若能透過對特徵的最大數量設限讓分支減少，就能夠降低過度擬合發生的可能性。

#### e. min_impurity_decreases
此參數是sklearn 中對impurity 最小值的限制，由於在sklearn 函式庫中，DecisionTreeClassifier 的criterion 有entrophy 及gini 兩種，而criterion = entrophy 時判斷節點分割的標準是資訊增益(Information gain) ，criterion = gini 時判斷節點分割的標準是不純度(Impurity) ，因此只有在criterion = gini 時，才能使用這項限制。sklearn 計算impurity_decreases 的公式如下方所示。

![SklearnImpurityDecreases_formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_prunning_preprunning_1.png?inline=false)

![SklearnImpurityDecreases_formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_prunning_preprunning_2.png?inline=false)

使用這項限制的話，如果計算出來的impurity_decreases 的值大於或等於設定值，節點才會進行分割。反之，impurity_decreases 的值小於設定值的話，該節點的分類工作就會停止。

### 2. 後剪枝(Post-prunning)
後剪枝指，在Decision Tree Classifier 完全分類完畢後，才回頭利用不同的方式檢視分類結果，並選擇出可以剪除的分支。後剪枝的方式很多，以下將針對誤差降低剪枝(Reduced Error Prunning, REP) 、悲觀剪枝(Pessimistic Error Prunning, PEP) 、代價複雜度剪枝(Cost Complexity Prunning, CCP) 等三種剪枝方式進行說明。

#### a. 誤差降低剪枝(Reduced Error Prunning, REP)
誤差降低剪枝是由下而上的剪枝方式，並且需要區分出訓練樣本和驗證樣本。誤差降低剪枝的第一步，是針對訓練樣本中預計剪枝的的各節點，由下而上檢視該節點底下的各子節點，並以各子節點內部的多數樣本為正確樣本，此時節點內若有其他種類的樣本，則裁定其為錯誤分類。接著，用驗證樣本進行Decision Tree 分類後，以訓練樣本得出的各節點的正確分類為基準，判斷驗證樣本的Decision Tree 中相同節點內出現的錯誤分配數量，並據此計算剪枝前和剪枝後的錯誤分配率，以下是計算剪枝前後錯誤分配率的公式。

![REP_formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_REP_1.png?inline=false)

![REP_formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_REP_2.png?inline=false)

取得剪枝前後的錯誤分配率後，判斷是否剪枝的標準為：剪枝後的錯誤分配率等於或小於剪枝前，則對預計剪枝的節點進行剪枝，使其成為葉子節點。

![REP_formula_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_REP_3.png?inline=false)

#### b. 悲觀剪枝(Pessimistic Error Prunning, PEP)
悲觀剪枝同樣是利用剪枝前後的錯誤分配率來判斷是否需要進行剪枝，但和前面提到的誤差降低剪枝不同，它是由上而下的剪枝方式，且不需要區分訓練樣本和驗證樣本。由於沒有利用不同的樣本來驗證，這種剪枝方式在計算每個節點的錯分率時，需要固定加上一個懲罰因子0.5 ，並藉由錯誤分配率取得錯誤分配期望值的標準差，計算錯誤分配率及取得標準差的公式如下方所示。

![PEP_formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_PEP_1.png?inline=false)

![PEP_formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_PEP_2.png?inline=false)

錯誤分配期望值的計算，則需要結合上方的錯誤分配率，公式如下。

![PEP_formula_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_PEP_3.png?inline=false)

![PEP_formula_4](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_PEP_4.png?inline=false)

取得剪枝前後的錯誤分配率及標準差後，判斷是否剪枝的標準為：若剪枝前錯誤分配的期望值加上標準差大於或等於剪枝後的錯誤分配期望值，則進行剪枝；反之，若剪枝前錯誤分配的期望值加上標準差小於剪枝後的錯誤分配期望值，則保留分支。

![PEP_formula_5](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_PEP_5.png?inline=false)

#### c. 代價複雜度剪枝(Cost Complexity Prunning, CCP)
代價複雜度剪枝在判斷是否剪枝的依據和運作方式上與前述兩者有較大的不同。首先，代價複雜度剪枝考量的不再僅是節點的錯誤分配率，而是代價複雜度參數(Cost Complexity Parameter) 以及重新帶入錯誤率(Resubstitution Error Rate) 。

再者，代價複雜度剪枝的運作主要分成兩個步驟：步驟一為對所有非樹葉節點計算代價複雜度參數，並從中找出代價複雜度參數最小的節點；步驟二則是以循環的方式，對每一次循環代價複雜度參數最小的節點做剪枝，直到剪到剩根節點為止，並將一系列剪枝的結果記錄下來，繼而從中找出最佳的Decision Tree 。而假使在運算過程中，遇到代價複雜度參數相同的情況，則針對樹葉節點較多的分支優先做剪枝。

以下是進行代價複雜度剪枝時，計算代價複雜度參數，以及重新帶入錯誤率的相關公式。

![CCP_formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_CCP_1.png?inline=false)

![CCP_formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_CCP_2.png?inline=false)

在利用sklearn 進行Decision Tree 分類時，DecisionTreeClassifier 亦有提供代價複雜度剪枝的參數ccp_alpha，可見於下方內容，若想參考該參數的相關介紹，請點[此處](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier)。
>class sklearn.tree.DecisionTreeClassifier(*, criterion='gini', splitter='best', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features=None, random_state=None, max_leaf_nodes=None, min_impurity_decrease=0.0, class_weight=None, ccp_alpha=0.0)

由上方可以看出，DecisionTreeClassifier 雖設有ccp_alpha 的參數，但它無法自行計算出成本複雜度參數，因此該參數需要使用者先透過其他方式計算出其最佳的成本複雜度參數，才能夠獲得經過成本複雜度剪枝的最佳化Decision Tree 。

### 3. Exmaple: 以不同後剪枝方法取得剪枝結果或參數
假設下方為完整Decision Tree 的一部分，並決定要以後剪枝的方式評估當中的節點T 是否進行剪枝。已知該資料中有x, y 兩種樣本，以(x, y) 表示各節點分類後的結果，各節點的右上角或左上角則以黑字和紅字分別標注節點代號及正確樣本（即節點中佔多數之樣本）。

![Example_Prunning_Situation_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Prunning_Situation_1.png?inline=false)

以下將說明執行誤差降低剪枝和悲觀剪枝後所得到的結論，而代價複雜度剪枝由於步驟上要對每個非樹葉節點計算，此處無法呈現，故呈現代價複雜度參數和重新帶入錯誤率之結果。

#### a. 誤差降低剪枝(Reduced Error Prunning, REP)
前面提到，誤差降低剪枝是一種由下而上，且需要區分訓練樣本和驗證樣本的剪枝法，因此在決定節點T 是否需要剪枝時，需要依序先確認驗證樣本中t<sub>1</sub> 和t<sub>2</sub> 的錯誤誤差剪枝結果，再依據前述結果進一步推定T 是否需要剪枝。驗證樣本擬合後的結果，以及對t<sub>1</sub> 和t<sub>2</sub> 進行錯誤誤差剪枝的過程如下所示。

![Example_Post-Prunning_REP_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_REP_1.png?inline=false)

![Example_Post-Prunning_REP_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_REP_2.png?inline=false)

由上可知，t<sub>1</sub> 在剪枝前後的錯誤分配率相等，而t<sub>2</sub> 剪枝後的錯誤分配率大於剪枝前，因此t<sub>1</sub> 會進行剪枝，t<sub>2</sub> 則維持原樣。此時，該部分的Decision Tree 會轉變成下方的狀態。

![Example_Post-Prunning_REP_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_REP_3.png?inline=false)

接著，我們計算T 剪枝前後的錯誤分配率。

![Example_Post-Prunning_REP_4](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_REP_4.png?inline=false)

由上可知，Ｔ 剪枝後的錯誤分配率大於剪枝前，因此T 若使用誤差降低剪枝的方式，將不會進行剪枝，該部分的Decision Tree 維持t<sub>1</sub> 剪枝後的狀態。

#### b. 悲觀剪枝(Pessimistic Error Prunning, PEP)
悲觀剪枝是由上而下的剪枝方法，因此判斷T 是否需要剪枝，是依據原本的分類結果估算出剪枝前後的錯誤分配率，從而確認是否需要剪枝。悲觀剪枝的運行結果如下所示。

![Example_Post-Prunning_PEP_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_PEP_1.png?inline=false)

![Example_Post-Prunning_PEP_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_PEP_2.png?inline=false)

由上可看出，剪枝前錯誤分配的期望值小於剪枝後，因此T 將不進行剪枝。

#### c. 代價複雜度剪枝(Cost Complexity Prunning, CCP)
由於代價複雜度剪枝，會對所有非樹葉節點計算其代價複雜度參數，以及重新帶入錯誤率，並自代價複雜度參數最小者循環進行剪枝，因此僅有一部分的Decision Tree 無法呈現代價複雜度剪枝的結果。因此，此處將針對非樹葉節點T 計算其成本複雜度參數，以及重新帶入錯誤率，計算所需的整體Decision Tree 樣本數假設為160 。計算過程如下所示。

![Example_Post-Prunning_CCP_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Post-prunning_CCP_1.png)

## D. 以Decision Tree 進行 Fakepoints dataset 的分類工作
以下我們利用Decision Tree 進行Fakepoints dataset 的分類，此次使用吉尼不純度(Gini Impurity) 作為Decision Tree Classifier 決定是否產生分支的判斷標準，並限制最大深度為15 ，並在第一次分類完成後，針對Decision Tree 進行後剪枝，整體操作過程如下。

### Step 1. 以Training set 產生一Decision Tree
下圖底色為Decision Tree 對Training set 進行預測所得出的類別邊界，圖中各點為Training set 的資料在圖中所佔據的位置，並以兩種顏色表示顯示不同類別之資料。此時，該Decision Tree Classifier 針對Training set 的分類成績為1.0 ，代表其能夠準確無誤的對Training set 進行分類，而圖中各點確實也落在同色系的色塊上。

![Example_Fakepoints_DecisionTree_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_1.png?inline=false)

下圖則為該Decision Tree Classifier 所產生的Decision Tree ，深度和分支非常的大。

![Example_Fakepoints_DecisionTree_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_2.png?inline=false)

### Step 2. 以Testing set 測試此Decision Tree Classifier 的成效
下圖由上而下，分別為前一階段所訓練出的Decision Tree Classifier 對Testing set 進行分類的結果，以及Training set 和Testing set 之正確類別顯示圖。

此時，Decision Tree Classifier 對Testing set 進行分類的分數為0.9444444444444444 ，代表可能有部分Testing set 的資料被該Decision Tree Classifier 分到錯誤的類別，不過比例很低。

![Example_Fakepoints_DecisionTree_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_3.png?inline=false)

### Step 3. 觀察ccp_alpha 和準確度之間的關係
為了進行後剪枝的工作，我們利用sklearn 內建的API ，取得在不同ccp_alpha 值之下Training set 和Testing set 的成績，各點的ccp_alpha 值和Training set/Testing set 成績結果如下所示。

>ccp_alphas:  [0.         0.0024     0.00261333 0.00263602 0.00515221 0.0145262 0.02262857 0.02309479 0.02564935 0.03584    0.05284741]  
>Train_scores:  [1.0, 0.9973333333333333, 0.9946666666666667, 0.992, 0.9893333333333333, 0.9733333333333334, 0.952, 0.944, 0.9066666666666666, 0.8853333333333333, 0.8506666666666667]  
>Test_scores:  [0.9523809523809523, 0.9523809523809523, 0.9523809523809523, 0.9523809523809523, 0.9603174603174603, 0.9365079365079365, 0.8968253968253969, 0.9126984126984127, 0.8650793650793651, 0.8492063492063492, 0.8095238095238095]

![Example_Fakepoints_DecisionTree_4](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_4.png?inline=false)

### Step 4. 選擇ccp_alpha值產生一新的Decision Tree
由上一步驟可知，在ccp_alpha = 0.00515221 ，Training set 和Testing set 的成績差距最小，因此我們嘗試將ccp_alpha 設為0.00515221 ，取得新的Decision Tree。

![Example_Fakepoints_DecisionTree_5](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_5.png?inline=false)

![Example_Fakepoints_DecisionTree_6](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_6.png?inline=false)

![Example_Fakepoints_DecisionTree_7](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/DecisionTree/Example_Fakepoints_DecisionTree_7.png?inline=false)

此時，利用該Decision Tree Classifier 對Training set 分類的成績略降至0.992 ，而對Testing set 分類的成績略上升到0.9523809523809523 。由上圖可看出，Decision Tree 的層數和分支顯著減少，但Testing set 分類的成績卻有所改善。

## E. Reference
Scikit-learn https://scikit-learn.org/stable/modules/tree.html
