import os
import sys
import re
import json
from collections import OrderedDict
import glob
import argparse
from mkloop import mkloop

def Argparse():

    example = (
        "Command Example: \n"
        "python3 autoeval.py --pic_name 20201104-part2 --tool ../../\n"
        )
        
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter, epilog=example)
    parser.add_argument("-p", "--pic_name", type=str, help="pic_name")
    parser.add_argument("-t", "--todo_list", type=str, help="todo_list")

    args = parser.parse_args()
    return args

def patternfind(pattern, string):

    pattern = re.compile(pattern)
    target = pattern.findall(string)
    if target:
        return target[0]

    return target

def copyfile(dest, caffemodel, deploy, example):
    os.system("cp {} {}".format(caffemodel, dest))
    os.system("cp {} {}".format(deploy, dest))
    os.system("cp -r {}/* {}".format(example, dest))
    os.system("cp run_convert.py {}".format(dest)) # my load config run.py

def run_convert(work_dest):
    pwd = os.getcwd()
    os.chdir(work_dest)
    os.system("python3 run_convert.py")
    os.system("sh clean.sh")
    os.chdir(pwd)

def copytoguardian(work_dest, guardian_path):

    cavalry = glob.glob(work_dest + "/cavalry*.bin")[0]
    priorbox = glob.glob(work_dest + "/priorbox*.bin")[0]
    cavalry_to = os.path.join(guardian_path, os.path.basename(cavalry))
    priorbox_to = os.path.join(guardian_path, os.path.basename(priorbox))

    os.system("cp {} {}".format(cavalry, cavalry_to))
    os.system("cp {} {}".format(priorbox, priorbox_to))

    return cavalry, priorbox

def toworking(file_path, pic_dataset, loopfile):

    print(file_path)
    
    dest = "./Working/AMBA3.0"
    example_path = "./Model_to_bin/example3.0/"
    config_template="configTemplate.json"


    model_name = os.path.basename(file_path)
    model_path = os.path.dirname(file_path)
    caffe_base = os.path.dirname(model_path)
    basesplit = caffe_base.split("/")
    deploy_path = os.path.join(caffe_base, "Models/deploy.prototxt")

    # diferent path from v5.1.1 and v6.1.1
    if not os.path.isfile(deploy_path):
        deploy_path = os.path.join(caffe_base, "v5.1.1/deploy.prototxt")
        version = "v5.1.1" 
        backbone = basesplit[-1].split("_")[0] 
        training_round = basesplit[-1].split("_")[1]

    else:
        version = basesplit[-3].split("_")[1]
        backbone = basesplit[-2]
        training_round = basesplit[-1] 

    # Delete redunduct word
    if "PrimaxSSD_" in backbone:
        target = patternfind(r".*PrimaxSSD_", backbone)
        backbone = backbone.replace(target, "")

    # Get resolution
    with open(deploy_path, "r") as f:
        deploy_name = f.readline()
        resolution = patternfind(r"\d+x\d+", deploy_name)
    
    networkname = "ssd_{}_{}".format(version, resolution)
    training_iter = str(int(patternfind(r"\d+", model_name))//1000) + "k"
    folder_1 = "PrimaxSSD_{}_{}".format(version, backbone)
    folder_2 = "{}_{}_{}_{}".format(networkname, backbone, training_round, training_iter)
    work_dest = os.path.join(dest, folder_1, folder_2)

    # If do some special methods like pruning, get the information here.
    additional = patternfind(r"-.*[.]", model_name)
    if additional:
        additional = additional.replace(".","")
        work_dest = os.path.join(work_dest, additional)

    print(work_dest)

    os.makedirs(work_dest, exist_ok=True)
    copyfile(dest=work_dest, caffemodel=file_path, deploy=deploy_path, example=example_path)

    # config file
    with open(config_template, "r") as f:
        config = json.load(f, object_pairs_hook=OrderedDict)
        print(config)
    config_file = os.path.join(work_dest,"convert_config.json")
    config["Network Name"] = networkname
    config["Input Width"] = resolution.split("x")[0]
    config["Input Height"] = resolution.split("x")[1]
    config["backbone"] = backbone
    config["caffemodel"] = model_name
    config["deploy"] = os.path.basename(deploy_path)
    config["training_round"] = training_round
    config["training_iter"] = training_iter
    config["img_path"] = "img/"
    config["dest"] = work_dest
    config["additional"] = additional
    with open(config_file,"w") as f:
        json.dump(config, f, indent=1)
        print("save:", config_file)

    run_convert(work_dest)

    guardian_path = os.path.join("toGuardianII", work_dest)
    os.makedirs(guardian_path, exist_ok=True)

    cavalry, priorbox = copytoguardian(work_dest, guardian_path)
    config["cavalry"] = cavalry
    config["priorbox"] = priorbox

    with open(config_file,"w") as f:
        json.dump(config, f, indent=1)
        print("save:", config_file)

    mkloop(loopfile, pic_dataset, config)


if __name__ == '__main__':

    args = Argparse()
    list_file = args.todo_list
    pic_dataset = args.pic_name

    loopfile = "{}_loop.sh".format(pic_dataset)
    if os.path.isfile(loopfile):
        os.remove(loopfile)

    if os.path.isdir("toGuardianII"):
        os.system("rm -rf toGuardianII")

    with open(list_file, "r") as f :
        modellist = f.readlines()
        for file_path in modellist:
            file_path = file_path.strip()
            if file_path:
                toworking(file_path, pic_dataset, loopfile)

    os.system("mv {} toGuardianII/".format(loopfile))
