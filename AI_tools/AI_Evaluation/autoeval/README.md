## 01_ToBin
#### Environment
10.1.15.86 上的 AMBASDK3.0 docker環境

#### python requirements
1. tqdm = 4.62.3
2. matplotlib = 2.2.2
3. pandas = 0.22.0

#### Easy Start
1. 確認環境(AMBASDK3.0)。 
2. sourceamba && source ~/.bashrc
3. 確認軟連結設定。 `(ln -s target linkname)`
4. 將 caffe 訓練 log 路徑新增至 todolog.txt。
5. run readlog.py 會生成 todomodel.txt 和整理好的CSV。
6. run toworking.py 會生成 Working 和 toGuardianII 兩個資料夾。

## 02_Inference
#### Environment
Guardian II 3.0.4 ↑

#### Easy Start
1. 將toGuardianII資料夾複製到sdcard。
2. 將loop.sh複製到圖片bin 資料夾。
3. 將Working 複製到/sdcard下。
4. run loop.sh
5. 等待script跑完 收集生成的rawlog


## 03_Evaluate
#### Environment
python >= 3.5

#### python requirements
1. pandas = 1.3.5
2. tqdm = 4.62.3
3. matplotlib = 3.5.1
4. scipy = 1.7.3
5. opencv = 4.5.5.62

#### Easy Start
1. 將生成的rawlog 複製到 03_Evaluation 資料夾。
2. 確認軟連結設定。
3. run autoeval.py 會生成 rawlog, ssdlog, result和 mAP 四個資料夾。


## Fill Excel
1. 收集 01_ToBin readlog.py 的CSV
2. 收集 03_Evaluate  result.csv 和 mAP.csv。
3. 填寫 excel。
