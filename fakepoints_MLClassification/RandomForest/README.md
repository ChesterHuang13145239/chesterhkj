# Random Forest

## A. 介紹
Random Forest Classifier 是Decision Tree Classifier 和整體(Ensemble) 方法的結合體，透過取得多個具有高變異性的深度Decision Tree 結果，並將這些結果平均從而建構一個更強固的模型(robust model) ，藉此降低一般化誤差和過度擬合發生的可能性。

## B. 演算方式
Random Forest CLassifier 的運作大致可以歸納為四個階段。首先，利用整體學習方法中裝袋法(Bagging) 的概念，從訓練資料集的所有樣本中取出數量為n 的隨機自助(bootstrap) 樣本，被選中的樣本在下一次抽樣時會被放回（即採放回式）。

第二步，利用Decision Tree Classifier 對隨機取出的自助樣本進行分。此步驟當中，其分類過程與一般的Decision Tree 大概相同，即對各節點隨機找出d 個特徵，接著透過資訊增益(Information Gain) 最大等目標函數，取得最佳的分類方式。

接著，Random Forest Classifier 會重複k 次前述的兩個步驟，從中取得k 個Decision Tree 分類結果。最後，Random Forest Classifier 會將取得的k 個Decision Tree 的結果彙整，並以多數決(Majority Voting) 的方式指定訓練資料集中各個樣本所屬的類別。

由上述內容可知，Random Forest CLassifier 包含了裝袋法、放回和不放回式抽樣、Decision Tree Classifier 以及多數決等的概念，以下將對這些概念做簡單的說明。

### 1. 裝袋法(Bagging)
裝袋法和整體方法間存在關連，整體當中可能會存在許多不同的分類器，而裝袋法的概念即是自原始的訓練資料集當中，重複進行特定數目樣本的放回式隨機抽樣（這些樣本又被稱為自助樣本），並利用這些自助樣本去讓整體中的個別分類器進行分類。當所有的個別分類器完成自身所接收樣本的分類工作後，便會透過下面要介紹的多數決來預測各樣本所屬的類別。

裝袋法的特點主要有二：一是，整體的個別分類器在運作過程中不會重複遇到相同的資料集；二是，由於採取放回式的取樣，每次取出的樣本可能存在一定比例的重複數據。

Random Forest Classifier 是裝袋法的特例，因為一般的裝袋法是利用多個不同的分類演算法來建立多個的分類器，但Random Forest Classifier 則是將不同訓練子集合藉由Decision Tree Classifier 所產生出的分類結果，作為一個獨立的分類器看待。

### 2. 放回與不放回式抽樣
放回式和不放回式抽樣的概念是相對的，採取放回式抽樣的樣本在每次抽樣過後的樣本總數皆相同，而抽樣取得的各子資料集間存在一定比例的重複資料，Random Forest Classifier 抽取自助樣本時採用的便是放回式抽樣。反之，樣本經過不放回式抽樣後，樣本總數會減少，之後再進行抽樣時，將不會取得相同的樣本，Random Forest Classifier 在運作Decision Tree Classifier 時，對於樣本的選擇即是一種不放回式抽樣，因為取出的特徵不會再放回。

### 3. Decision Tree Classifier
關於Decision Tree Classifier 的相關介紹，請點[此處](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/src/DecisionTrees/README.md)。

### 4. 多數決(Majority Voting)
多數決的運作主要建立在整合方法中，將多個不同的分類器結合成一個整合分類器的背景下，透過彙集整合分類器中個別分類器對樣本種類所做出的預測，來決定我們最終對該資料集內各樣本種類的分類。在二元分類的情況下，若一樣本在個別分類器中獲得的預測有一半以上屬於某類別，則該樣本即被分入該類別當中。而若是有超過兩種類別的話，則會將獲得最多票數的類別作為對該樣本最後的預測。表示簡單多數決概念的公式如下所示。

![MajorityVoting_formula_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_1.png?inline=false)

![MajorityVoting_formula_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_2.png?inline=false)

而若是在二元分類工作中，將class 1 訂為-1 ，class 2 訂為+1 ，則多數決的預測亦可如下表示。

![MajorityVoting_formula_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_3.png?inline=false)

除了簡單多數決外，多數決在實際應用中，也可以結合權重的概念去做決策，搭配權重的加權多數決公式如下所示。

![MajorityVoting_formula_4](fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_4.png?inline=false)

![MajorityVoting_formula_5](fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_5.png?inline=false)

### 5. Example: 多數決的實際運作
假設在一Random Forest Classifier 中，有3 個個別分類器針對10 個相同的樣本進行分類，並限制其Decision Tree 的深度，使之分類後的各自產生的Decision Tree 以及對樣本分類的結果如下所示。

![MajorityVoting_formula_6](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_6.png)

![MajorityVoting_formula_7](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_7.png)

此時，可用前述的多數決公式，計算出個別的樣本被預測的分類結果，如下所示。

![MajorityVoting_formula_8](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_8.png)

![MajorityVoting_formula_9](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_9.png)

### 6. Example: 簡單多數決和加權多數決對分類結果之影響
假設今天要利用一用有三個個別分類器的整體分類器，來預測一樣本x 的類別是屬於0 或1 。以下是三個分類器各自預測的結果。

![MajorityVoting_formula_10](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_10.png)

若是買個分類器的權重皆相同，那我們可以使用簡單多數決的公式，計算過程如下，由此可知x 應該屬於類別0 。

![MajorityVoting_formula_11](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_11.png)

但若是三個個別分類器的信心加權依序是0.2, 0.2, 0.6 的話，利用加權多數決計算的過程如下，此時x 會被分入類別1 當中。

![MajorityVoting_formula_12](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_MajorityVoting_12.png)

## C. Sklearn 的RandomForestClassifier 參數
以下是Sklearn 中RandomForestClassifier 參數，其中也包含許多與Decision Tree Classifier 相同的參數，關於網上的參數説明請點[這裡](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier)。

>class sklearn.ensemble.RandomForestClassifier(n_estimators=100, *, criterion='gini', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto', max_leaf_nodes=None, min_impurity_decrease=0.0, bootstrap=True, oob_score=False, n_jobs=None, random_state=None, verbose=0, warm_start=False, class_weight=None, ccp_alpha=0.0, max_samples=None)。

## D. 以Random Forest 進行 Fakepoints dataset 的分類工作
以下我們利用Random Forest 進行Fakepoints dataset 的分類，此次分類的分類器設定為50 個，並使用吉尼不純度(Gini Impurity) 作為當中各Decision Tree Classifier 決定是否產生分支的判斷標準，整體操作過程如下。

### Step 1. 以Training set 產生一Random Forest
下圖底色為Random Forest Classifier 對Training set 進行預測所得出的類別邊界，圖中各點為Training set 的資料在圖中所佔據的位置，並以兩種顏色表示顯示不同類別之資料。此時，該Random Forest Classifier 針對Training set 的分類成績為1.0 ，代表其能夠準確無誤的對Training set 進行分類，而圖中各點確實也落在同色系的色塊上。

![Example_Fakepoints_RandomForest_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_Fakepoints_RandomForest_1.png?inline=false)

### Step 2. 以Testing set 測試此Random Forest Classifier 的成效
下圖由上而下，分別為前一階段所訓練出的Random Forest Classifier 對Testing set 進行分類的結果，以及Training set 和Testing set 之正確類別顯示圖。

此時，Random Forest Classifier 對Testing set 進行分類的分數為0.9920634920634921 ，代表可能有部分Testing set 的資料被該Random Forest Classifier 分到錯誤的類別，不過比例很低。

![Example_Fakepoints_RandomForest_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/RandomForest/Example_Fakepoints_RandomForest_2.png?inline=false)

## C. Reference
Scikit-learn https://scikit-learn.org/stable/modules/ensemble.html#forests-of-randomized-trees
