import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.tree import DecisionTreeClassifier, plot_tree

C1_TRAIN_X = os.path.join(os.getcwd(), "../Datasets/Fakepoints_datasets", "c1_train.txt")
C2_TRAIN_X = os.path.join(os.getcwd(), "../Datasets/Fakepoints_datasets", "c2_train.txt")
C1_TEST_X  = os.path.join(os.getcwd(), "../Datasets/Fakepoints_datasets", "c1_test.txt")
C2_TEST_X  = os.path.join(os.getcwd(), "../Datasets/Fakepoints_datasets", "c2_test.txt")

def readData(filename, y):

    with open(filename, "r") as f:
        lines     = f.readlines()
        rows      = len(lines)
        datamat_x = np.zeros((rows, 2))
        datamat_y = np.zeros(rows, dtype = int)
 
        for i in range(rows):
            line = lines[i].rstrip("\n").split(" ")
            x1   = float(line[0])
            x2   = float(line[1])
            datamat_x[i] = [x1, x2]
            datamat_y[i] = int(y)

    return datamat_x, datamat_y

def collectDataAll(datamat_a, datamat_b):

    datamat_ab = np.concatenate([datamat_a, datamat_b])

    return datamat_ab

def findMaxmin(train_x):

    x1_max = train_x[:, 0].max() + 1
    x1_min = train_x[:, 0].min() - 1
    x2_max = train_x[:, 1].max() + 1
    x2_min = train_x[:, 1].min() - 1

    return x1_max, x1_min, x2_max, x2_min


def plot_class_regions(tree, x1_coordinate, x2_coordinate, x, y):

    fig, ax = plt.subplots()
    y_class_proba = tree.predict_proba(np.c_[x1_coordinate.ravel(), x2_coordinate.ravel()])[:, 1]
    y_class_proba = y_class_proba.reshape(x1_coordinate.shape)
    ax.contourf(x1_coordinate, x2_coordinate, y_class_proba, cmap=cm, alpha=0.6)
    ax.scatter(x[:, 0], x[:, 1], c = y, cmap = cm_bright, edgecolors= "k")
    ax.set_title("Result of classification toward training set with Decision Tree")
    train_score = tree.score(c1c2_train_x, c1c2_train_y)
    print("The score of training set: ", train_score)
    plt.show()
    
    return y_class_proba

def show_classification_results(tree, x1_coordinate, x2_coordinate,\
                                test_x, test_y, train_x, train_y):

    y_class_proba = plot_class_regions(tree, x1_coordinate, x2_coordinate,\
                                       train_x, train_y)
    test_y_pred = tree.predict(test_x)

    plot_tree(tree, filled=True)
    plt.show()

    fig, ax = plt.subplots(2, 1)
    ax[0].contourf(x1_coordinate, x2_coordinate, y_class_proba, cmap=cm, alpha=0.6)
    ax[0].scatter(test_x[:, 0], test_x[:, 1], c = test_y_pred, cmap = cm_bright, edgecolors= "k")
    ax[0].set_title("Result of classification toward testing set with Decision Tree")
    test_score = tree.score(c1c2_test_x, c1c2_test_y)
    print("The score of testing set: ", test_score)
    ax[1].contourf(x1_coordinate, x2_coordinate, y_class_proba, cmap=cm, alpha=0.6)
    ax[1].scatter(train_x[:, 0], train_x[:, 1], c = train_y, cmap = cm_bright, edgecolors= "k")
    ax[1].scatter(test_x[:, 0], test_x[:, 1], c = test_y, cmap = cm_bright, alpha=0.4, edgecolors= "k")
    ax[1].set_title("The correct class to training/testing set")
    fig.tight_layout()
    plt.show()

def findccp_alpha(tree, train_x, train_y, test_x, test_y):
    prunning_path = tree.cost_complexity_pruning_path(train_x, train_y)
    ccp_alphas    = prunning_path.ccp_alphas

    dt_withccpalphas = []
    for ccp_alpha in ccp_alphas:
        dt_withccpalpha = DecisionTreeClassifier(ccp_alpha=ccp_alpha, criterion='gini', max_depth=15)
        dt_withccpalpha.fit(train_x, train_y)
        dt_withccpalphas.append(dt_withccpalpha)

    dt_withccpalphas = dt_withccpalphas[:-1]
    ccp_alphas       = ccp_alphas[:-1]
    train_scores = [dt_withccpalpha.score(train_x, train_y) for dt_withccpalpha in dt_withccpalphas]
    test_scores  = [dt_withccpalpha.score(test_x, test_y) for dt_withccpalpha in dt_withccpalphas]
    print("ccp_alphas: ", ccp_alphas)
    print("Train_scores: ", train_scores)
    print("Test_scores: ", test_scores)

    fig, ax = plt.subplots()
    ax.set_xlabel("alpha")
    ax.set_ylabel("accuracy")
    ax.set_title("Accuracy vs alpha for training and testing sets")
    ax.plot(ccp_alphas, train_scores, marker="o", label="train", drawstyle="steps-post")
    ax.plot(ccp_alphas, test_scores, marker="o", label="test", drawstyle="steps-post")
    ax.legend()
    plt.show()

if __name__ == "__main__":

    c1_train_x, c1_train_y = readData(C1_TRAIN_X, 0)
    c2_train_x, c2_train_y = readData(C2_TRAIN_X, 1)
    c1_test_x, c1_test_y   = readData(C1_TEST_X, 0)
    c2_test_x, c2_test_y   = readData(C2_TEST_X, 1)

    c1c2_train_x = collectDataAll(c1_train_x, c2_train_x)
    c1c2_train_y = collectDataAll(c1_train_y, c2_train_y)
    c1c2_test_x  = collectDataAll(c1_test_x, c2_test_x)
    c1c2_test_y  = collectDataAll(c1_test_y, c2_test_y)
    c1c1_all_x   = collectDataAll(c1c2_train_x, c1c2_test_x)
#    print("c1c2_train_x: ", c1c2_train_x)
#    print("c1c2_train_y: ", c1c2_train_y)
#    print("c1c2_test_x: ", c1c2_test_x)
#    print("c1c1_all_x: ", c1c1_all_x)

    dt = DecisionTreeClassifier(ccp_alpha=0.0, criterion='gini', max_depth=15)
    dt.fit(c1c2_train_x, c1c2_train_y)

    x1_max, x1_min, x2_max, x2_min = findMaxmin(c1c2_train_x)
    x1_coordinate, x2_coordinate   = np.meshgrid(np.arange(x1_min, x1_max, 0.02),
                                                 np.arange(x2_min, x2_max, 0.02))
#    print("x1_coordinate: ", x1_coordinate)
#    print("x2_coordinate: ", x2_coordinate)

    cm = plt.cm.RdBu
    cm_bright = ListedColormap(['#FF0000', '#0000FF'])

    show_classification_results(dt, x1_coordinate, x2_coordinate, c1c2_test_x, c1c2_test_y,\
                                c1c2_train_x, c1c2_train_y)
    
    findccp_alpha(dt, c1c2_train_x, c1c2_train_y, c1c2_test_x, c1c2_test_y)

    dt2 = DecisionTreeClassifier(ccp_alpha = 0.00515221, criterion='gini', max_depth=15)
    dt2.fit(c1c2_train_x, c1c2_train_y)
    show_classification_results(dt2, x1_coordinate, x2_coordinate, c1c2_test_x, c1c2_test_y,\
                                c1c2_train_x, c1c2_train_y)
