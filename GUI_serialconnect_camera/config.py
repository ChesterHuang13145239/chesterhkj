import serial

POC_MFG_API = {\
"SYSTEM":{"MF mode\nchange":\
           [1, "XM", "MFMODE", "0~", "", "", "Null", "Null", "Null"],\
           "Help":\
           [2, "XM", "HELP", "", "", "", "Null", "Null", "Null"],\
           "Software\nVersion":\
           [3, "XM", "SWVER", "", "", "", "Null", "Null", "Null"]},\
 "GPIO":{"NTC":\
         [4, "XM", "NTC", "0", "", "", "Null", "Null", "Null"],\
         "HWID":\
         [5, "XM", "HWID", "", "", "", "Null", "Null", "Null"],\
         "RGB LED":\
         [6, "XM", "RGBLED", "R strength 0~100",\
          "G strength 0~100", "B strength 0~100", "Null", "Null", "Null"],\
         "IR":\
         [7, "XM", "IR", "0", "IR strength 0~100", "", "Null", "Null", "Null"],\
         "Reset to\ndefault button":\
         [8, "XM", "RESET_\nBUTTON", "", "", "", "Null", "Null", "Null"]},\
 "AUDIO":{"Audio":\
          [9, "XM", "AUDIO", "", "", "", "Null", "Null", "Null"],\
          "Play 1k\nHz tone":\
          [10, "XM", "PLAY1K", "0 or 1", "", "", "Null", "Null", "Null"],
          "Adjust\nspeaker\nvolume":
          [11, "XM", "AUDIO_VOL", "0~175", "", "", "Null", "Null", "Null"]},\
 "WIRELESS":{"WiFi":\
             [12, "XM", "WIFI", "SSID", "PASSWORD", "", "Null", "Null", "Null"],\
             "Bluetooth":\
             [13, "XM", "BT", "", "", "", "Null", "Null", "Null"],\
             "RF":\
             [14, "XM", "RF", "", "", "", "Null", "Null", "Null"],\
             "IPREF":\
             [15, "XM", "IPREF", "PORT", "", "", "Null", "Null", "Null"]},\
 "FLASH":{"FLASH":\
           [16, "XM", "FLASH", "", "", "", "Null", "Null", "Null"]},\
 "RAM":{"RAM":\
        [17, "XM", "RAM", "", "", "", "Null", "Null", "Null"]}}

SERIAL_CONNECTION_SETTINGS = \
{"Baud rate": ["9600", "19200", "38400", "57600", "115200"],\
 "Data bits": ["5", "6", "7", "8"],\
 "Parity": ["None", "Odd", "Even", "Mark", "Space"],\
 "Stop bits": ["1", "1.5", "2"],\
 "Flow control": ["None", "XON", "XOFF"]}

SERIAL_CONNECTION_DEFAULT_SETTINGS = ["115200", "8", "None", "1", "None"]

DATABITS_INPUT_VALUE = {"5": serial.FIVEBITS, "6": serial.SIXBITS, \
                        "7": serial.SEVENBITS, "8": serial.EIGHTBITS}
PARITY_INPUT_VALUE = {"None": serial.PARITY_NONE, "Odd": serial.PARITY_ODD , \
                      "Even": serial.PARITY_EVEN , "Mark": serial.PARITY_MARK, \
                      "Space": serial.PARITY_SPACE}
STOPBITS_INPUT_VALUE = {"1": serial.STOPBITS_ONE, "1.5": serial.STOPBITS_ONE_POINT_FIVE, \
                        "2": serial.STOPBITS_TWO}
FLOWCONTROL_INPUT_VALUE = {"None": False, "XON": True, "XOFF": True}

class SystemSetting:

    def __init__(self):
        self.port = None
        self.baudrate = None
        self.databits = None
        self.parity   = None
        self.stopbits = None
        self.flowcontrol = None
        self.connectionstatus = False
        self.settingvalues = [self.port, "115200", "8", "None", \
                              "1", "None", self.connectionstatus]
        self.setiings = ["Port", "Baud rate", "Data bits", "Parity", "Stop bits", \
                         "Flow control", "Connection status"]

        self.serial = None
        
    def collectSettingValue(self, itemnum, settingvalue):

        self.settingvalues[itemnum] = settingvalue
