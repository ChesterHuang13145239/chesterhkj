# K-Nearest Neighbor(KNN)

## A. 介紹
K-Nearest Neighbor Classifier 是一項監督式學習法，也是惰式學習器的典型案例，概念是通過測量不同樣本在特徵空間內的距離進行分類，其運作主要建立在無母數模型和惰式學習器的特點上，以下將簡單介紹這兩者的概念。

### 1. 無母數模型(Nonparametric Model)
無母數模型(Nonparametric Model) 是與有母數模型(Parametric Model) 相對的一個概念。這種模型的特徵是不固定的，且特徵數量會隨著資料集的收集而變多，因此無法用一組固定的特徵來做描述，因此每次有新資料加入，都必須將之前收集的資料全部重新讀取，才能完成新資料的分類。相對的，使用有母數模型的分類器，則會從訓練資料集中取得參數值，從而獲得一函數來幫助其進行分類，因此不需要舊的訓練資料集即可進行新資料的分類工作。

Decision Tree 和Random Forest 等分類演算法都是無母數模型的案例。而K-Nearest Neighbor 由於每次運算都會將所有資料集記憶起來，因此也是無母數模型的應用。

### 2. 惰式學習器(Lazy Learner)
惰式學習器(Lazy Learner) 最大的特徵，就是它不會在訓練的過程中習得一判別函數，而是將整個訓練資料集記憶起來，如K-Nearest Neighbor 即會將所讀取的資料集在特徵空間內的位置全部記錄下來，並據此對新資料作出判斷。

正因前述兩點的基礎架構，K-Nearest Neighbor Classifier 具有能在收集訓練資料集的同時，同步更新分類器的優點，但也造就其計算複雜度有很大的可能會隨資料夾的收集而線性增加的缺點。

## B. 演算方式
K-Nearest Neighbor Classifier 的演算方式是，將訓練資料集的資料位置記錄在特徵空間中後，一旦有新加入的資料，就會計算該資料與特徵空間內所有資料的距離，從而找出該資料在特徵空間中最鄰近的k 個樣本。加入這k 個樣本中的多數為特定類別，則新加入的樣本即被分入該類別，意即K-Nearest Neighbor Classifier 在定類決策上只依據最鄰近的一個或者幾個樣本的類別來決定新加入樣本所屬的類別。

綜上所述，利用K-Nearest Neighbor Classifier 進行分類工作時，主要有計算距離（給定新加入資料的資料位置與距離計算基準，並計算它與訓練資料集的每筆資料的距離）、尋找近鄰（圈定距離最近的k 個物件，作為新加入資料的近鄰）、進行分類（在k 個近鄰中找出多數的類別，將新加入的資料分入該類別）三個步驟。在這些環節當中，距離的計算和k 值的設定是影響分類結果的重要因素。

### 1. 距離的計算
K-Nearest Neighbor Classifier 在距離的計算上，常用的有歐氏距離(Euclidean distance) 、曼哈頓距離(Manhattan distance) 等，兩者的計算方式有明顯的差異，以下將介紹兩種距離的計算方式，以及sklearn 中有關距離計算的設定參數。

#### a. 歐氏距離(Euclidean distance)

![DisatnceCalculation_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_DistanceCalculation_1.png?inline=false)

#### b. 曼哈頓距離(Manhattan distance)

![DisatnceCalculation_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_DistanceCalculation_2.png?inline=false)

根據以上對歐氏距離和曼哈頓距離的介紹，上述兩種距離的計算方式可以整合成下方的此一公式。

![DisatnceCalculation_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_DistanceCalculation_3.png?inline=false)

在上方的公式中，用來確認使用的距離計算方式為歐式距離還是曼哈頓距離之關鍵，在於P 的數值。當P = 1 時，所使用的是曼哈頓距離，當P = 2 時，所使用的則是歐氏距離。

下方為sklearn 中KNeighborsClassifier 的參數內容，當中也設有距離計算方式的選項p ，預設值為p = 2 ，代表預設的距離為歐氏距離， 若要改用曼哈頓距離，只需改為p = 1 即可。
>class sklearn.neighbors.KNeighborsClassifier(n_neighbors=5, *, weights='uniform', algorithm='auto', leaf_size=30, p=2, metric='minkowski', metric_params=None, n_jobs=None)

### 2. Example: K-Nearest Neighbor Classifier 的運行
假設下圖為訓練資料集和新加入資料在特徵空間內的位置分布關係，圖中符號各自代表分入不同類別的資料及新加入的資料，距離的計算採用歐氏距離，並假定k = 5 ，則K-Nearest Neighbor Classifier 的運算方式過程如下所示。

![K-NearesrNeighbor_progressing_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_KNNprocessing_1.png?inline=false)

#### a. 計算距離
在尋找近鄰之前，K-Nearest Neighbor Classifier 會計算圖中新資料和周圍所有資料之間的距離，並記錄起來。

#### b. 尋找近鄰
此次分類工作設定的k 值為5 ，因此K-Nearest Neighbor Classifier 會從記錄下來的資料當中，找出與新加入的資料距離最接近的五筆資料。

![K-NearesrNeighbor_progressing_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_KNNprocessing_2.png?inline=false)

#### c. 進行分類
被匡列出來的五筆資料，其類別的統計結果如下表所示，由此可知在歐氏距離和k = 5 的條件下，新加入的資料會被分入Type 1 的類別。

![K-NearesrNeighbor_progressing_3](fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_KNNprocessing_3.png?inline=false)

### 3. Example: k 值的影響

k 值的大小決定K-Nearest Neighbor Classifier 選取的最近鄰數量，由於分類是依據k 個最近鄰資料中的多數類別，因此k 值的大小可能會對結果產生影響。前段已取得以歐氏距離為距離單位，並且假定k = 5 時的分類結果，下方則是我們將k 值改為 7 時的分類結果。

![K-NearesrNeighbor_K-influence_3](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_KNNprocessing_4.png?inline=false)     

對於k = 5 時的結果，當時新資料被分入Type 1 類別，同一筆新資料在k = 7 時卻被分入Type 2 類別。由此可看出，透過K-Nearest Neighbor Classifier 進行分類時，同一筆新加入的資料，可能會因k 值的大小而影響分類的結果。

## C. Sklearn 的KNeighborsClassifier 參數
以下是Sklearn 中RandomForestClassifier 參數，關於網上的參數説明請點[這裡](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier)。

>class sklearn.neighbors.KNeighborsClassifier(n_neighbors=5, *, weights='uniform', algorithm='auto', leaf_size=30, p=2, metric='minkowski', metric_params=None, n_jobs=None)

## D. 以K-Nearest Neighbor 進行 Fakepoints dataset 的分類工作
以下我們利用K-Nearest Neighbor 進行Fakepoints dataset 的分類，此次分類k 值設定為3 ，並使用歐式距離(Euclidean distance) 作為距離計算的單位，並依據距離遠近給予依據k 值所選出的各點不同的權重，越近的點權重越大，整體操作過程如下。

### Step 1. 以Training set 產生一Random Forest
下圖底色為K-Nearest Neighbor Classifier 對Training set 進行預測所得出的類別邊界，圖中各點為Training set 的資料在圖中所佔據的位置，並以兩種顏色表示顯示不同類別之資料。此時，該RK-Nearest Neighbor Classifier 針對Training set 的分類成績為1.0 ，代表其能夠準確無誤的對Training set 進行分類，而圖中各點確實也落在同色系的色塊上。

![Example_Fakepoints_knn_1](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_Fakepoints_knn_1.png?inline=false)

### Step 2. 以Testing set 測試此K-Nearest Neighbor Classifier 的成效
下圖由上而下，分別為前一階段所訓練出的K-Nearest Neighbor Classifier 對Testing set 進行分類的結果，以及Training set 和Testing set 之正確類別顯示圖。

此時，K-Nearest Neighbor Classifier 對Testing set 進行分類的分數為0.9920634920634921 ，代表可能有部分Testing set 的資料被該K-Nearest Neighbor Classifier 分到錯誤的類別，不過比例很低。

![Example_Fakepoints_knn_2](https://gitlab.com/ChesterHuang13145239/chesterhkj/-/raw/main/fakepoints_MLClassification/example/images/formula/NearestNeighbors/Example_Fakepoints_knn_2.png?inline=false)

## E. Reference
Scikit-learn https://scikit-learn.org/stable/modules/neighbors.html