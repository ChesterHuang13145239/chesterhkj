# 02_Inference

複製 01_ToBin 最後所生成的toGuardianII資料夾內的資料到sdcard上

### 步驟一：複製檔案
將{pic_name}\_loop.sh複製到PIC_BIN內

#### 檔案放置
+ /sdcard
	+ PIC_BIN
		+ {pic_name}
			+ {resolution}
				+ dra_bin0/
				+ Run_bins.sh
				+ {pic_name}\_loop.sh
	+ Working
	    * AMBA3.0
	        * PrimaxSSD\_{version}\_{backbone}
	            * {networkname}\_{backbone}\_{training_round}\_{training_iter}
					+ priorbox_fp32.bin
					+ cavalry_ssd_{version}\_{resolution}\_{backbone}-{training_round}-{training_iter}.bin

### 步驟二：Run loop.sh
```
./20201104-part2_loop.sh
```

>耐心等待

輸出：rawlog

