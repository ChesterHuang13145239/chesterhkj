# ws_GUI

# A. Intorduction
This program is a GUI designed to coontrol IP Camera through serial connection, and save system logs as .txt file.

# B. Dependencies
Python>=3.8  
PyQt5>=5.15

# C. Execution
After download the repository with git command, type following command on the commandline.

> python3 main.py

# D. Introduction to Files
The whole application is divided into four parts: main.py, workthread.py, config.py and gui_controller.py.

## 1. main.py
This file controls the whole workflow and feature, and the flow chart of the workflow is shown below.

## 2. config.py
This file contains basic configuration of both IP camera and serial connection, and uses class called Systemsetting to control serial connection.

## 3. gui_controller.py
This file contains the classes which control the appearance of GUI.  
  
The whole GUI is composed of System Setting which is used to set up serial connection, AT Command Tab which is used to set up configuration of IP Camera and Log Window which is used to keep showing logs since the serial connection starts.

## 4. workthread.py
If the GUI developed with PyQt5 needs to send messages or data through different widgets, QThread and PyqtSignal will be necessary modules at the time.  
  
This file contains features of getting information ports, opening serial port, closing serila port, sending/receiving serial data, saving logs, and sending corresponding messages to Log Window.